#ifndef CORE_QMLMENU_H
#define CORE_QMLMENU_H

#include "core_QMLMenu_global.h"
#include "../interface/interface_QMLMenu.h"
#include "qmlmenu_listmodel.h"

#include <QQmlEngine>

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QByteArray>
#include <QObject>

class CORE_QMLMENU_EXPORT Core_QMLMenu : public QObject,Interface_QMLMenu
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Rocowolf.QMLMenu")
    Q_INTERFACES(Interface_QMLMenu)

public:
    Core_QMLMenu();

    QJsonObject vfunc_GetInformation() override;
    int vfunc_Run() override;
    int vfunc_Exit() override;
    void vfunc_Debug() override;
    bool vfunc_Vertify(QPluginLoader* p_loader) override;

    void vfunc_InitQML(const QJsonArray& p_arr) override;
    void vfunc_LoadQML(const QJsonArray& p_arr) override;
    void vfunc_UnloadQML(const QJsonArray& p_arr) override;

private:
    QJsonObject prop_Info;
    QMLMenu_ListModel* prop_Model = nullptr;
};

#endif // CORE_QMLMENU_H
