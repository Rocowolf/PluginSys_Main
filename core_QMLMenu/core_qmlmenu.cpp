#include "core_qmlmenu.h"

Core_QMLMenu::Core_QMLMenu()
{
    this->prop_Info.insert("plugin_name","core_QMLMenu");
    this->prop_Info.insert("plugin_version","1.0.D");
    this->prop_Info.insert("plugin_dependence", QJsonArray());
    // QJsonObject m_qml;
    // m_qml.insert("qml_name","菜单");
    // m_qml.insert("qml_qmlurl","qrc:/QMLMenu/MyItem.qml");
    // m_qml.insert("qml_pluginname",this->prop_Info["plugin_name"]);
    // this->prop_Info.insert("plugin_qml", m_qml);

    this->prop_Model = QMLMenu_ListModel::sfunc_GetInstance();
    qmlRegisterSingletonInstance<QMLMenu_ListModel>("QMLMenu_ListModel",1,0,"QMLMenu_ListModel",this->prop_Model);
}

QJsonObject Core_QMLMenu::vfunc_GetInformation()
{
    return this->prop_Info;
}

int Core_QMLMenu::vfunc_Run()
{
    qDebug() << "[core_QMLMenu] Run";
    return 0;
}

int Core_QMLMenu::vfunc_Exit()
{
    qDebug() << "[core_QMLMenu] Exit";
    return 0;
}

void Core_QMLMenu::vfunc_Debug()
{
    qDebug().noquote() << "==========================================================";
    qDebug().noquote() << tr("[%1] DEBUG").arg(this->prop_Info["plugin_name"].toString());
    qDebug().noquote() << tr("插件版本: %1").arg(this->prop_Info["plugin_version"].toString());
    qDebug().noquote() << tr("插件依赖项:");
    QJsonArray m_arr = this->prop_Info["plugin_dependence"].toArray();
    if(m_arr.isEmpty()){
        qDebug().noquote() << tr("\t无");
    }
    else{
        foreach(auto m_item, m_arr){
            qDebug().noquote() << tr("\t%1").arg(m_item.toString());
        }
    }
    qDebug().noquote() << tr("插件QML信息:");
    if(this->prop_Info["plugin_qml"].toObject().isEmpty()){
        qDebug().noquote() << tr("\t无");
    }
    else{
        QJsonObject m_obj = this->prop_Info["plugin_qml"].toObject();
        for(auto m_item = m_obj.begin(); m_item != m_obj.end(); ++m_item){
            qDebug().noquote() << "\t" << m_item.key() << ":" << m_item.value().toString();
        }
    }
    qDebug().noquote() << "==========================================================";
}

bool Core_QMLMenu::vfunc_Vertify(QPluginLoader* p_loader)
{
    return (qobject_cast<Interface_QMLMenu*>(p_loader->instance()) != nullptr);
}

void Core_QMLMenu::vfunc_InitQML(const QJsonArray &p_arr)
{
    if(this->prop_Model != nullptr && !p_arr.isEmpty()){
        this->prop_Model->func_Init(p_arr);
    }
}

void Core_QMLMenu::vfunc_LoadQML(const QJsonArray &p_arr)
{
    if(this->prop_Model != nullptr && !p_arr.isEmpty()){
        this->prop_Model->func_AppendItems(p_arr);
    }
}

void Core_QMLMenu::vfunc_UnloadQML(const QJsonArray &p_arr)
{
    if(this->prop_Model != nullptr && !p_arr.isEmpty()){
        this->prop_Model->func_RemoveItems(p_arr);
    }
}
