#ifndef QMLMENU_LISTMODEL_H
#define QMLMENU_LISTMODEL_H

#include <QAbstractListModel>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QResource>

class QMLMenu_ListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit QMLMenu_ListModel(QObject *parent = nullptr);
    static QMLMenu_ListModel* sfunc_GetInstance();

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QHash<int,QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void func_Init(const QJsonArray& p_arr);
    void func_AppendItems(const QJsonArray& p_arr);
    void func_RemoveItems(const QJsonArray& p_arr);

    Q_INVOKABLE void func_Debug();
    // Q_INVOKABLE void func_Reset();

signals:
    void sig_TEST_Unload(QString p_name);

private:
    static QMLMenu_ListModel* sprop_Instance;
    QHash<int,QByteArray> prop_HashMap;
    QMap<QString,QJsonObject*> prop_PluginMap;
    QList<QJsonObject*> prop_PluginList;
};

#endif // QMLMENU_LISTMODEL_H
