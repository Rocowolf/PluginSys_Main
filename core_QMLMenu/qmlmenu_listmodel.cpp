#include "qmlmenu_listmodel.h"

QMLMenu_ListModel::QMLMenu_ListModel(QObject *parent)
    : QAbstractListModel(parent)
{
    this->prop_HashMap.insert(0,"qml_pluginname");
    this->prop_HashMap.insert(1,"qml_name");
    this->prop_HashMap.insert(2,"qml_imgurl");
    this->prop_HashMap.insert(3,"qml_qmlurl");

    // QJsonObject* m_obj = new QJsonObject;
    // m_obj->insert("qml_pluginname","plugin_QMLMenu");
    // m_obj->insert("qml_name","插件信息");
    // m_obj->insert("qml_imgurl","qrc:/QMLMenu/info.png");
    // m_obj->insert("qml_qmlurl","qrc:/QMLMenu/info.qml");
    // this->prop_PluginList.append(m_obj);

//    qDebug() << "[TEST] " << QResource::registerResource("../source/myresource.rcc");
}

QMLMenu_ListModel* QMLMenu_ListModel::sfunc_GetInstance()
{
    if(QMLMenu_ListModel::sprop_Instance == nullptr){
        QMLMenu_ListModel::sprop_Instance = new QMLMenu_ListModel;
    }
    return QMLMenu_ListModel::sprop_Instance;
}

int QMLMenu_ListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
    return this->prop_PluginList.count();
}

QHash<int, QByteArray> QMLMenu_ListModel::roleNames() const
{
    return this->prop_HashMap;
}

QVariant QMLMenu_ListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    return this->prop_PluginList[index.row()]->value(this->prop_HashMap[role]);
    // return QVariant();
}

void QMLMenu_ListModel::func_Init(const QJsonArray& p_arr)
{
    if(p_arr.isEmpty()){
        return;
    }
    foreach(auto m_item, p_arr){
        QJsonObject* m_obj = new QJsonObject(m_item.toObject());
        if(m_obj->value("qml_pluginname").toString("") == ""){
            delete m_obj;
            m_obj = nullptr;
            continue;
        }
        this->prop_PluginList.append(m_obj);
        this->prop_PluginMap.insert(m_obj->value("qml_pluginname").toString(), m_obj);
    }
}

void QMLMenu_ListModel::func_AppendItems(const QJsonArray& p_arr)
{
    if(p_arr.isEmpty()){
        return;
    }
    int m_count = p_arr.count();
    this->beginInsertRows(QModelIndex(), this->prop_PluginList.count(), this->prop_PluginList.count() + m_count - 1);
    foreach(auto m_item, p_arr){
        QJsonObject* m_obj = new QJsonObject(m_item.toObject());
        if(m_obj->value("qml_pluginname").toString("") == ""){
            delete m_obj;
            m_obj = nullptr;
            continue;
        }
        this->prop_PluginList.append(m_obj);
        this->prop_PluginMap.insert(m_obj->value("qml_pluginname").toString(), m_obj);
    }
    this->endInsertRows();
}

void QMLMenu_ListModel::func_RemoveItems(const QJsonArray &p_arr)
{
    // qDebug() << "[core_QMLMenu] " << p_arr;
    // qDebug() << "[core_QMLMenu] " << this->prop_PluginMap.keys();
    if(p_arr.isEmpty()){
        return;
    }
    foreach(auto m_item, p_arr){
        QString p_name = m_item.toString("");
        if(p_name == ""){
            continue;
        }
        if(this->prop_PluginMap.contains(p_name)){
            // qDebug() << "[core_QMLMenu] contains";
            int m_index = this->prop_PluginList.indexOf(this->prop_PluginMap.take(p_name));
            if(m_index == -1){
                continue;
            }
            this->beginRemoveRows(QModelIndex(), m_index, m_index);
            auto m_item = this->prop_PluginList.takeAt(m_index);
            this->endRemoveRows();
            delete m_item;
        }
    }
}

void QMLMenu_ListModel::func_Debug()
{
    // qDebug() << "HelloWorld";
}

// void QMLMenu_ListModel::func_Reset()
// {
//     qDebug() << this->prop_PluginMap.keys();
//     // qDebug() << "[TEST] " << QResource::unregisterResource("../source/myresource.rcc");
//     this->beginResetModel();
//     this->endResetModel();
// }

QMLMenu_ListModel* QMLMenu_ListModel::sprop_Instance = nullptr;
