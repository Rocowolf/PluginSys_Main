import QtQuick 2.15
import QtQuick.Controls
import QMLMenu_ListModel 1.0
import Qt5Compat.GraphicalEffects

Rectangle {
    id: root
    color: "skyblue"
    property real prop_Ratio: 0.1
    property real prop_ToolBarMaxHeight: 60
    property int prop_IconWidth: 80
    property int prop_IconHeight: 110
    property int prop_IconSpacing: 70

    property GridView prop_Menu: GridView {
        id: gridview

        cellWidth: root.prop_IconWidth + root.prop_IconSpacing
        cellHeight: root.prop_IconHeight + root.prop_IconSpacing

        model: QMLMenu_ListModel
        delegate: Rectangle {
            id: outline
            property Loader prop_SubItem: Loader{
                source: qml_qmlurl
                active: true
            }

            width: gridview.cellWidth
            height: gridview.cellHeight
            color: Qt.rgba(0,0,0,0)

            Rectangle {
                width: parent.width * 0.75
                height: parent.height * 0.75
                color: mousea.containsMouse ? Qt.rgba(0.8,0.8,0.8,0.5) : Qt.rgba(0,0,0,0)
                anchors.centerIn: parent
                radius: 10
                clip: true

                MouseArea {
                    id: mousea

                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: containsMouse ? Qt.PointingHandCursor :  Qt.ArrowCursor

                    onClicked: {
                        stackview.prop_EnterX = outline.width/2 + outline.x
                        stackview.prop_EnterY = outline.height/2 + outline.y
                        stackview.push(outline.prop_SubItem)
                    }

                    Rectangle {
                        id: outline2

                        width: root.prop_IconWidth
                        height: root.prop_IconHeight
                        anchors.centerIn: parent
                        color: Qt.rgba(0,0,0,0)

                        Column {
                            Image {
                                width: outline2.width
                                height: outline2.height - 30
                                source: qml_imgurl === undefined ? "qrc:/QMLMenu/plugin.png" : qml_imgurl
                                mipmap:true
                                fillMode: Image.PreserveAspectFit

                            }

                            Text {
                                width: outline2.width
                                height: 30
                                text: qml_name
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                fontSizeMode: Text.Fit
                                font.pixelSize: 30
                            }
                        }
                    }
                }
            }
        }
    }

    StackView {
        property real prop_EnterX: 0
        property real prop_EnterY: 0

        id: stackview
        width: parent.width
        height: parent.height - toolbar.height
//        anchors.fill: parent

        initialItem: root.prop_Menu

        pushEnter: Transition {
            ParallelAnimation{
                ScaleAnimator{
                    from: 0
                    to: 1
                    duration: 400
                    easing.type: Easing.OutCubic
                }
                XAnimator{
                    from: -stackview.width/2 + stackview.prop_EnterX
                    to: 0
                    duration: 400
                    easing.type: Easing.OutCubic
                }
                YAnimator{
                    from: -stackview.height/2 + stackview.prop_EnterY
                    to: 0
                    duration: 400
                    easing.type: Easing.OutCubic
                }
            }
        }

        popExit: Transition {
            ParallelAnimation{
                ScaleAnimator{
                    from: 1
                    to: 0
                    duration: 400
                    easing.type: Easing.OutCubic
                }
                XAnimator{
                    from: 0
                    to: -stackview.width/2 + stackview.prop_EnterX
                    duration: 400
                    easing.type: Easing.OutCubic
                }
                YAnimator{
                    from: 0
                    to: -stackview.height/2 + stackview.prop_EnterY
                    duration: 400
                    easing.type: Easing.OutCubic
                }
            }
        }

        popEnter: Transition {
////            XAnimator {
////                from: (stackview.mirrored ? -1 : 1) * -stackview.width
////                to: 0
////                duration: 400
////                easing.type: Easing.OutCubic
////            }
            ParallelAnimation{
                ScaleAnimator{
                    from: 0
                    to: 1
                    duration: 400
                    easing.type: Easing.OutCubic
                }
                XAnimator{
                    from: -stackview.width/2
                    to: 0
                    duration: 400
                    easing.type: Easing.OutCubic
                }
                YAnimator{
                    from: -stackview.height/2
                    to: 0
                    duration: 400
                    easing.type: Easing.OutCubic
                }
            }
        }
    }

    Rectangle {
        id: toolbar

        width: parent.width
        height: Math.min(prop_ToolBarMaxHeight,parent.height * root.prop_Ratio)
        anchors.bottom: parent.bottom
        color: "gray"

        // Rectangle {
        //     width: row.implicitWidth
        //     height: row.implicitHeight
        //     anchors.centerIn: parent

            Row {
                id: row
                anchors.centerIn: parent
                spacing: 20

                Button {
                    width: 60
                    height: toolbar.height - 10
                    text: "功能"

                    // onClicked: {
                    //     QMLMenu_ListModel.func_Reset();
                    // }
                }

                Button {
                    width: 60
                    height: toolbar.height - 10
                    text: "主页"
                }

                Button {
                    width: 60
                    height: toolbar.height - 10
                    text: "回退"

                    onClicked: {
                        stackview.pop()
                    }
                }
            }
        // }
    }
}
