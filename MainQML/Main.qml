import QtQuick
import QtQuick.Controls

Window {
    width: 640
    height: 480
    minimumWidth: 640
    minimumHeight: 480
    visible: true
    title: qsTr("Rocowolf")
    color: "pink"

    Loader{
        anchors.fill: parent
        source: "qrc:/QMLMenu/MyItem.qml"
        active: true
    }
}
