#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QCoreApplication>

#include <QThread>

#include <QJsonDocument>
#include "class_pluginmanager.h"

int main(int argc, char *argv[])
{
//    int* p = new int(5);
//    QSharedPointer<int> m_list(p, [](int* p_obj){
//        qDebug() << "DELETE";
//        delete p_obj;
//    });
//    m_list.clear();
//    qDebug() << *p;
//    return 0;

    if(argc >= 2 && argv[1] == std::string("--console")){
        QCoreApplication app(argc, argv);
        int m_ret = class_PluginManager::sfunc_GetInstance(0)->prop_State;
        if(m_ret < 0){
            return m_ret;
        }

        return app.exec();
    }
    else{
        QApplication app(argc, argv);

        int m_ret = class_PluginManager::sfunc_GetInstance(1)->prop_State;
        if(m_ret < 0){
            return m_ret;
        }

        // QThread* obj_WorkThread = new QThread;
        QObject::connect(&app, &QApplication::aboutToQuit, class_PluginManager::sfunc_GetInstance(1), &class_PluginManager::slot_Debug);
        // QObject::connect(class_PluginManager::sfunc_GetInstance(1), &QObject::destroyed, obj_WorkThread, &QThread::quit);
        // obj_WorkThread->start();
        // class_PluginManager::sfunc_GetInstance(1)->moveToThread(obj_WorkThread);
        // class_PluginManager::sfunc_GetInstance(1)->func_SetWorkThread(obj_WorkThread);

        QQmlApplicationEngine engine;
        const QUrl url(u"qrc:/MainQML/Main.qml"_qs);
        QObject::connect(
            &engine,
            &QQmlApplicationEngine::objectCreationFailed,
            &app,
            []() { QCoreApplication::exit(-1); },
            Qt::QueuedConnection);

        engine.load(url);

        return app.exec();
    }
}
