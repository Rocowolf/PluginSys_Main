#ifndef CLASS_PLUGINMANAGER_H
#define CLASS_PLUGINMANAGER_H

#include <QObject>
#include <QPluginLoader>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QMessageBox>
#include <QRegularExpression>
#include <QPointer>
#include <QSharedPointer>
#include <QThread>
#include <QFileSystemWatcher>
#include <QResource>

#include "../interface/interface_ServerPlugin.h"
#include "../interface/interface_DependenceCheck.h"
#include "../interface/interface_QMLMenu.h"

#include <QQmlApplicationEngine>

class class_PluginManager : public QObject
{
    Q_OBJECT
public:
    explicit class_PluginManager(QObject *parent = nullptr);
    class_PluginManager(bool p_isUseGUI, QObject *parent = nullptr);
    ~class_PluginManager();

    Q_INVOKABLE void func_Debug();

    static class_PluginManager* sfunc_GetInstance(bool p_isUseGUI);
    void func_SetWorkThread(QThread* p_workthread);
    int prop_State = 0;

public slots:
    void slot_Install(QString p_name);
    void slot_Uninstall(QString p_name);
    void slot_Debug();

private:
    QDir prop_PluginDir;
    QDir prop_CoreDir;

    QMap<QString, QSharedPointer<QPluginLoader>> prop_LoaderMap;
    QStringList prop_FileList;

    Interface_QMLMenu* prop_QMLMenu;
    Interface_DependenceCheck* prop_DependenceCheck;
    Interface_ServerPlugin* prop_ServerPlugin;

    QRegularExpression prop_CoreEXP;
    QRegularExpression prop_PluginEXP;

    QFileSystemWatcher prop_FileWatcher;

    static class_PluginManager* sprop_Instance;

signals:
    void sig_AboutToQuit();
};

#endif // CLASS_PLUGINMANAGER_H
