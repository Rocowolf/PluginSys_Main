import QtQuick 2.15
import MainQML_ListModel 1.0

Rectangle {
    id: root

    color: "gray"

    ListView{
        id: listview

        width: parent.width * 0.9
        height: parent.height * 0.9
        anchors.centerIn: parent
        spacing: 20
        clip: true

        model: MainQML_ListModel
        delegate: Rectangle{
            id: outline

            color: Qt.rgba(0.8,0.8,0.8,1)
            width: listview.width
            height: outline2.implicitHeight

            Column {
                id: outline2

                Text {
                    width: outline.width
                    height: 20
                    text: qml_name
                }

                Text {
                    width: outline.width
                    height: 20
                    text: "无"
                }
            }
        }
    }
}
