#include <iostream>

#include "../interface/interface_Base.h"
#include "../interface/interface_UserBase.h"

#include "class_pluginmanager.h"

class_PluginManager::class_PluginManager(QObject *parent)
    : QObject{parent}
{

}

class_PluginManager::class_PluginManager(bool p_isUseGUI, QObject *parent)
{
    QDir m_dir = QDir::current();
    this->prop_CoreEXP.setPattern("^core_.*");
    this->prop_PluginEXP.setPattern("^plugin_.*");
    QList<Interface_Base*> m_coreplugin;
    QJsonObject m_plugininfo;

//核心插件载入开始=================================================================================================

    if(!m_dir.cd("../core")){
        if(!p_isUseGUI){
            qDebug() << "[错误]:未找到核心插件文件夹";
            this->prop_State = -1;
            return;
        }
        QMessageBox::critical(nullptr,"错误","未找到核心插件文件夹",QMessageBox::Ok);
        this->prop_State = -1;
        return;
    }
    this->prop_CoreDir = m_dir;

    if(p_isUseGUI){
        QPluginLoader* m_loader = new QPluginLoader(this->prop_CoreDir.absoluteFilePath("core_QMLMenu.dll"));
        if(m_loader->load()){
            auto m_interface = qobject_cast<Interface_QMLMenu*>(m_loader->instance());
            if(m_interface != nullptr && m_interface->vfunc_Vertify(m_loader)){
                auto m_info = m_interface->vfunc_GetInformation();
                m_plugininfo.insert("core_QMLMenu.dll", m_info);
                this->prop_LoaderMap.insert("core_QMLMenu.dll",QSharedPointer<QPluginLoader>(m_loader, [this](QPluginLoader* obj){
                                                this->prop_QMLMenu->vfunc_Exit();
                                                this->prop_QMLMenu = nullptr;
//                                                obj->unload();
                                                obj->deleteLater();
                                            }));
                m_coreplugin.append(m_interface);
                this->prop_QMLMenu = m_interface;
            }
            else{
                m_loader->unload();
                m_loader->deleteLater();
                QMessageBox::critical(nullptr,"错误","核心插件 core_QMLMenu.dll 接口错误",QMessageBox::Ok);
                this->prop_State = -1;
                return;
            }
        }
        else{
            m_loader->deleteLater();
            QMessageBox::critical(nullptr,"错误","核心插件 core_QMLMenu.dll 未找到",QMessageBox::Ok);
            this->prop_State = -1;
            return;
        }
    }

    QPluginLoader* m_loader = new QPluginLoader(this->prop_CoreDir.absoluteFilePath("core_DependenceCheck.dll"));
    if(m_loader->load()){
        auto m_interface = qobject_cast<Interface_DependenceCheck*>(m_loader->instance());
        if(m_interface != nullptr && m_interface->vfunc_Vertify(m_loader)){
            QJsonObject m_obj;
            m_obj.insert("core_QMLMenu.dll",QJsonArray({}));
            m_obj.insert("core_DependenceCheck.dll",QJsonArray({}));
            m_obj.insert("core_ServerPlugin.dll",QJsonArray({}));
            m_interface->vfunc_LoadPlugin(m_obj);
            auto m_info = m_interface->vfunc_GetInformation();
            m_plugininfo.insert("core_DependenceCheck.dll", m_info);
            this->prop_LoaderMap.insert("core_DependenceCheck.dll",QSharedPointer<QPluginLoader>(m_loader, [this](QPluginLoader* obj){
                                            this->prop_DependenceCheck->vfunc_Exit();
                                            this->prop_DependenceCheck = nullptr;
//                                            obj->unload();
                                            obj->deleteLater();
                                        }));
            m_coreplugin.append(m_interface);
            this->prop_DependenceCheck = m_interface;
        }
        else{
            m_loader->unload();
            m_loader->deleteLater();
            QMessageBox::critical(nullptr,"错误","核心插件 core_DependenceCheck.dll 接口错误",QMessageBox::Ok);
            this->prop_State = -1;
            return;
        }
    }
    else{
        m_loader->deleteLater();
        QMessageBox::critical(nullptr,"错误","核心插件 core_DependenceCheck.dll 未找到",QMessageBox::Ok);
        this->prop_State = -1;
        return;
    }

    m_loader = new QPluginLoader(this->prop_CoreDir.absoluteFilePath("core_ServerPlugin.dll"));
    if(m_loader->load()){
        auto m_interface = qobject_cast<Interface_ServerPlugin*>(m_loader->instance());
        if(m_interface != nullptr && m_interface->vfunc_Vertify(m_loader)){
            auto m_info = m_interface->vfunc_GetInformation();
            m_plugininfo.insert("core_ServerPlugin.dll", m_info);
            this->prop_LoaderMap.insert("core_ServerPlugin.dll",QSharedPointer<QPluginLoader>(m_loader, [this](QPluginLoader* obj){
                                            this->prop_ServerPlugin->vfunc_Exit();
                                            this->prop_ServerPlugin = nullptr;
//                                            obj->unload();
                                            obj->deleteLater();
                                        }));
            m_coreplugin.append(m_interface);
            this->prop_ServerPlugin = m_interface;
            connect(m_loader->instance(), SIGNAL(vsig_Install(QString)), this, SLOT(slot_Install(QString)));
            connect(m_loader->instance(), SIGNAL(vsig_Uninstall(QString)), this, SLOT(slot_Uninstall(QString)));
        }
        else{
            m_loader->unload();
            m_loader->deleteLater();
            QMessageBox::critical(nullptr,"错误","核心插件 core_ServerPlugin.dll 接口错误",QMessageBox::Ok);
            this->prop_State = -1;
            return;
        }
    }
    else{
        m_loader->deleteLater();
        QMessageBox::critical(nullptr,"错误","核心插件 core_ServerPlugin.dll 未找到",QMessageBox::Ok);
        this->prop_State = -1;
        return;
    }

    qDebug() << "[完成] 核心插件预加载插件完成";

//核心插件载入结束=================================================================================================

//功能插件预载入开始================================================================================================

    if(!m_dir.cd("../plugins")){
        if(!p_isUseGUI){
            qDebug() << "[警告]:未找到插件文件夹";
            qDebug() << "是否选择无插件启动? [Y/N]";
            while(1){
                std::string m_cin;
                std::cin >> m_cin;
                if(m_cin == std::string("Y") || m_cin == std::string("y")){
                    this->prop_State = 1;
                    return;
                }
                else if(m_cin == std::string("N") || m_cin == std::string("n")){
                    this->prop_State = -1;
                    return;
                }
                else{
                    qDebug() << "输入错误，请输入Y(y)或者N(n)";
                }
            }
        }
        auto m_select = QMessageBox::warning(nullptr,"警告","未找到插件文件夹\n是否选择无插件启动?",QMessageBox::Yes | QMessageBox::No);
        if(m_select == QMessageBox::Yes){
            this->prop_State = 1;
            return;
        }
        else{
            this->prop_State = -1;
            return;
        }
    }
    else{
        this->prop_PluginDir = m_dir;
        if(prop_FileWatcher.addPath(this->prop_PluginDir.path())){
            connect(&this->prop_FileWatcher, &QFileSystemWatcher::directoryChanged, this, [this](){
                this->prop_FileList =QDir(this->prop_PluginDir).entryList(QDir::Files | QDir::NoDotAndDotDot);;
            });
        }

        //插件载入
        foreach(auto m_item, m_dir.entryInfoList()){
            if(m_item.suffix() == "dll" && m_item.baseName().contains(this->prop_PluginEXP)/* && m_item.baseName() == "plugin_SerialServer"*/){
                QPluginLoader* m_loader = new QPluginLoader(m_item.absoluteFilePath());
                if(m_loader->load()){
                    Interface_UserBase* m_interface = qobject_cast<Interface_UserBase*>(m_loader->instance());
                    if(m_interface->vfunc_Vertify(m_loader)){
                        QString m_name = m_item.fileName();
                        m_plugininfo.insert(m_item.fileName(), m_interface->vfunc_GetInformation());
                        this->prop_LoaderMap.insert(m_item.fileName(),QSharedPointer<QPluginLoader>(m_loader, [this,m_name,m_interface](QPluginLoader* obj){
                                                        m_interface->vfunc_Exit();
                                                        this->prop_QMLMenu->vfunc_UnloadQML({m_name});
                                                        // qDebug() << "卸载是否成功:" << obj->unload();
                                                        obj->deleteLater();
                                                        this->prop_ServerPlugin->vsig_Uninstalled(m_name, true);
                                                    }));
                    }
                    else{
                        m_loader->unload();
                        m_loader->deleteLater();
                    }
                }
                else{
                    QMessageBox::warning(nullptr,"提示",m_loader->errorString(),QMessageBox::Yes);
                    m_loader->deleteLater();
                }
            }
        }
        qDebug() << "[完成] 预加载插件完成";

//功能插件预载入结束=================================================================================================

//功能插件依赖项检测开始=================================================================================================
        QJsonObject m_param;
        for(auto m_item = this->prop_LoaderMap.begin(); m_item != this->prop_LoaderMap.end(); ++m_item){
            if(m_item.key().contains(this->prop_CoreEXP)){
                continue;
            }
            m_param.insert(m_item.key(), qobject_cast<Interface_UserBase*>(m_item.value()->instance())->vfunc_GetInformation().value("plugin_dependence"));
        }
        m_param = this->prop_DependenceCheck->vfunc_LoadPlugin(m_param).toObject();
        if(!m_param.isEmpty()){
            QString m_warnning = "";
            foreach(auto m_item, m_param.value("failed").toArray()){
                m_plugininfo.remove(m_item.toString());
                this->prop_LoaderMap.remove(m_item.toString());
                m_warnning.append(QString("%1\n").arg(m_item.toString()));
            }
            if(m_warnning != ""){
                if(!p_isUseGUI){
                    qDebug() << "[警告]:以下插件未被加载:";
                    qDebug() << m_warnning + "是否继续启动? [Y/N]";
                    while(1){
                        std::string m_cin;
                        std::cin >> m_cin;
                        if(m_cin == std::string("Y") || m_cin == std::string("y")){
                            this->prop_State = 1;
//                            return;
                        }
                        else if(m_cin == std::string("N") || m_cin == std::string("n")){
                            this->prop_State = -1;
                            return;
                        }
                        else{
                            qDebug() << "输入错误，请输入Y(y)或者N(n)";
                        }
                    }
                }
                else{
                    auto m_select = QMessageBox::warning(nullptr, "警告", "以下插件未被加载:\n" + m_warnning + "是否继续启动?", QMessageBox::Yes | QMessageBox::No);
                    if(m_select == QMessageBox::No){
                        this->prop_State = -1;
                        return;
                    }
                }
            }
        }
        this->prop_FileList = QDir(this->prop_PluginDir).entryList(QDir::Files | QDir::NoDotAndDotDot);
        qDebug() << "[完成] 插件依赖项检测";
    }
//功能插件依赖项检测结束=================================================================================================

//功能插件依赖树构造开始=================================================================================================

    for(auto m_item = this->prop_LoaderMap.begin(); m_item != this->prop_LoaderMap.end(); ++m_item){
        if(!m_item.key().contains(this->prop_PluginEXP)){
            continue;
        }
        auto m_instance = m_item.value()->instance();
        connect(m_instance, SIGNAL(vsig_Unload(QString)), this, SLOT(slot_Uninstall(QString)));
        auto m_obj = qobject_cast<Interface_UserBase*>(m_instance);
        m_obj->vfunc_Init(&this->prop_LoaderMap);
    }
    this->prop_ServerPlugin->vfunc_Init(&this->prop_LoaderMap, &this->prop_FileList);
    qDebug() << "[完成] 依赖树构建";

//功能插件依赖树构造结束=================================================================================================

//页面初始化加载开始=================================================================================================

    if(p_isUseGUI && this->prop_State != 2 && this->prop_State != 1 && !this->prop_LoaderMap.isEmpty()){
        QJsonArray m_arr;
        for(auto m_item = m_plugininfo.begin(); m_item != m_plugininfo.end(); ++m_item) {
            if(m_item.value().toObject().contains("plugin_qml")){
                QJsonObject m_data = m_item.value().toObject().value("plugin_qml").toObject();
                m_data.insert("qml_pluginname", m_item.key());
                m_arr.append(m_data);
            }
        }
        this->prop_QMLMenu->vfunc_InitQML(m_arr);
    }
    qDebug() << "[完成] 所有预加载";

//页面初始化加载结束=================================================================================================

//页面初始化加载开始=================================================================================================

    foreach(auto m_item, m_coreplugin){
        m_item->vfunc_Run();
    }
    for(auto m_item = this->prop_LoaderMap.begin(); m_item != this->prop_LoaderMap.end(); ++m_item){
        if(m_item.key().contains(this->prop_CoreEXP)){
            continue;
        }
        qobject_cast<Interface_UserBase*>(m_item.value()->instance())->vfunc_Run();
    }
    qDebug() << "[完成] 插件初始化";

//    QString p_ret;
//    foreach(auto m_item, this->prop_LoaderMap.keys()){
//        p_ret.append(QString("%1\n").arg(m_item));
//    }
//    QMessageBox::warning(nullptr,"提示",p_ret,QMessageBox::Yes);

//页面初始化加载开始=================================================================================================
}

class_PluginManager::~class_PluginManager()
{
    qDebug() << "[Manager] 主体析构";
    this->prop_LoaderMap.clear();
}

void class_PluginManager::func_Debug()
{

}

class_PluginManager* class_PluginManager::sfunc_GetInstance(bool p_isUseGUI)
{
    if(class_PluginManager::sprop_Instance == nullptr){
        class_PluginManager::sprop_Instance = new class_PluginManager(p_isUseGUI);
    }
    return class_PluginManager::sprop_Instance;
}

void class_PluginManager::func_SetWorkThread(QThread* p_workthread)
{
}

void class_PluginManager::slot_Install(QString p_name)
{
    auto m_item = QFile(this->prop_PluginDir.absoluteFilePath(p_name));
    // qDebug() << "[Manager] 文件是否存在:" << m_item.exists();
    if(m_item.exists()){
        auto m_info = QFileInfo(m_item);

        if(this->prop_LoaderMap.contains(p_name)){
            this->prop_ServerPlugin->vsig_Installed(p_name, false);
            return;
        }
        QPluginLoader* m_loader = new QPluginLoader(m_info.absoluteFilePath());
        if(m_loader->load()){
            Interface_UserBase* m_plugin = qobject_cast<Interface_UserBase*>(m_loader->instance());
            if(m_plugin != nullptr && m_plugin->vfunc_Vertify(m_loader)){
                // qDebug() << "[DEBUG] 转换成功";
                QJsonObject m_param;
                QJsonObject m_plugininfo = m_plugin->vfunc_GetInformation();
                m_param.insert(p_name, m_plugin->vfunc_GetInformation().value("plugin_dependence"));
                if(this->prop_DependenceCheck->vfunc_LoadPlugin(m_param).toObject().value("success").toArray().isEmpty()){
                    // qDebug() << "[DEBUG] 检查失败";
                    m_loader->deleteLater();
                    this->prop_ServerPlugin->vsig_Installed(p_name, false);
                }
                else{
                    this->prop_LoaderMap.insert(p_name, QSharedPointer<QPluginLoader>(m_loader, [this,p_name,m_plugin](QPluginLoader* obj){
                                                    m_plugin->vfunc_Exit();
                                                    this->prop_QMLMenu->vfunc_UnloadQML({p_name});
//                                                    obj->unload();
                                                    obj->deleteLater();
                                                    this->prop_ServerPlugin->vsig_Uninstalled(p_name, true);
                                                }));
                    if(m_plugininfo.contains("plugin_qml")){
                        QJsonObject m_data;
                        m_data = m_plugininfo.value("plugin_qml").toObject();
                        m_data.insert("qml_pluginname",p_name);
                        this->prop_QMLMenu->vfunc_LoadQML(QJsonArray({m_data}));
                    }
                    m_plugin->vfunc_Run();
                    this->prop_ServerPlugin->vsig_Installed(p_name, true);
                }
            }
            else{
                // qDebug() << "[DEBUG] 转换失败";
                m_loader->deleteLater();
                this->prop_ServerPlugin->vsig_Installed(p_name, false);
            }
        }
        else{
            // qDebug() << "[DEBUG] 加载失败";
            m_loader->deleteLater();
            this->prop_ServerPlugin->vsig_Installed(p_name, false);
        }
    }
    else{
        this->prop_ServerPlugin->vsig_Installed(p_name, false);
        return;
    }
}

void class_PluginManager::slot_Uninstall(QString p_name)
{
    if(this->prop_LoaderMap.contains(p_name)){
        this->prop_QMLMenu->vfunc_UnloadQML(QJsonArray({p_name}));
        this->prop_LoaderMap.remove(p_name);
        qDebug() << QFile(this->prop_PluginDir.filePath(p_name)).remove();
        this->prop_ServerPlugin->vsig_Uninstalled(p_name, true);
    }
}

void class_PluginManager::slot_Debug()
{
    auto m_keylist = this->prop_LoaderMap.keys();
    foreach(auto m_item, m_keylist){
        if(m_item.contains(this->prop_CoreEXP)){
            continue;
        }
        this->prop_LoaderMap.remove(m_item);
    }
    this->prop_LoaderMap.clear();
}

class_PluginManager* class_PluginManager::sprop_Instance = nullptr;
