#ifndef CLASS_TASK_H
#define CLASS_TASK_H

#include <QObject>
#include <QHostAddress>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QTcpSocket>
#include <QFile>
#include <QMutex>
#include <QTimer>
//#include <QThread>
#include "class_thread.h"

class class_Task : public QObject
{
    Q_OBJECT
public:
    explicit class_Task(QObject *parent = nullptr);

    enum enum_TaskType{
        E_Data,
        E_File
    };
    Q_ENUM(enum_TaskType)

    enum enum_TaskStatus{
        E_Default,
        E_Abort,
        E_Connect,
        E_Finished,
        E_Quit
    };

    bool func_Init(const QHostAddress& p_ip, const quint16 p_port,
                   enum_TaskType p_type, const QJsonObject& p_task);

    QString prop_SourceName;

signals:
    void sig_Finished(QString p_name, QByteArray p_data);
//    void sig_NextTask(QThread* p_workthread);
    void sig_Aborted(QString p_name);
    void sig_Update(QString p_name, qint64 p_cursize, qint64 p_totalsize);

public slots:
    void slot_Run(class_Thread* p_workthread);
    void slot_Abort(QString p_name);
    void slot_Finished();

    void slot_Connected();
    void slot_Disconnected();
    void slot_ReadyRead();

private:
    QTcpSocket* prop_Socket = nullptr;
    QString prop_TargetPath;
    QFile* prop_Target = nullptr;
    enum_TaskType prop_TaskType;
    class_Thread* prop_WorkThread = nullptr;
    QHostAddress prop_Ip;
    quint16 prop_Port;

    // QString prop_SourceName;
    qint64 prop_SourceSize = 0;
    qint64 prop_CurrentSize = 0;
    QByteArray prop_Buffer;

    enum_TaskStatus prop_TaskStatus = E_Default;
};

#endif // CLASS_TASK_H
