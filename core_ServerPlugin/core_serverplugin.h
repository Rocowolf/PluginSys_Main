#ifndef CORE_SERVERPLUGIN_H
#define CORE_SERVERPLUGIN_H

#include "core_ServerPlugin_global.h"
#include "../interface/interface_ServerPlugin.h"
#include "servermodel.h"
#include "class_task.h"
#include "class_thread.h"

#include <QQuickWidget>
#include <QObject>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QTcpSocket>
//#include <QThread>
#include <QByteArray>
#include <QtConcurrent>

class CORE_SERVERPLUGIN_EXPORT Core_ServerPlugin : public QObject,Interface_ServerPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Rocowolf.ServerPlugin")
    Q_INTERFACES(Interface_ServerPlugin)

public:
    Core_ServerPlugin();
    ~Core_ServerPlugin();

    QJsonObject vfunc_GetInformation() override;
    int vfunc_Run() override;
    int vfunc_Exit() override;
    void vfunc_Debug() override;
    bool vfunc_Vertify(QPluginLoader* p_loader) override;

    void vfunc_Init(const QMap<QString, QSharedPointer<QPluginLoader>>* p_pluginmap, const QStringList* p_filelist) override;

    Q_INVOKABLE bool func_Download(QString p_name, bool p_isfile);

signals:
    void vsig_Downloaded(QString p_name) override;
    void vsig_Buffered(QString p_name, QByteArray p_data) override;
    void vsig_Aborted(QString p_name) override;
    void vsig_Update(QString p_name, qint64 p_current, qint64 p_total) override;
    void vsig_Installed(QString p_name, bool p_state) override;
    void vsig_Uninstalled(QString p_name, bool p_state) override;

    void vsig_Install(QString p_name) override;
    void vsig_Uninstall(QString p_name) override;
    void vsig_Download(QString p_name) override;
    void vsig_Abort(QString p_name) override;

    void sig_Run(class_Thread* p_workthread);

public slots:
    void slot_TaskFinished(class_Thread* p_workthread);
    void slot_Debug();

private:
    QJsonObject prop_Info;

    ServerModel* prop_ServerModel = nullptr;

    bool prop_AboutToClose = false;
    QList<class_Thread*> prop_BusyThreadList;
    QList<class_Thread*> prop_SpareThreadList;
    QList<class_Task*> prop_TaskList;

    QHostAddress prop_ServerIp = QHostAddress("101.43.219.247");
    // QHostAddress prop_ServerIp = QHostAddress("192.168.31.57");

    quint16 prop_ServerPort = 8123;
    // quint16 prop_ServerPort = 62000;
    QString prop_LocalPath ="D:/Proj/Qt6/Rocowolf7/plugins/";
};

#endif // CORE_SERVERPLUGIN_H
