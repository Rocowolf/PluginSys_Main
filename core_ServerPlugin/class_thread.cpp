#include "class_thread.h"

class_Thread::class_Thread(QObject* parent)
    : QThread{parent}
{
    this->prop_Timer = new QTimer();
    connect(this->prop_Timer, SIGNAL(timeout()), this, SLOT(slot_TimeOut()), Qt::DirectConnection);
    connect(this, SIGNAL(sig_StartTimer()), this->prop_Timer, SLOT(start()), Qt::QueuedConnection);
    connect(this, SIGNAL(sig_StopTimer()), this->prop_Timer, SLOT(stop()), Qt::QueuedConnection);
    connect(this, SIGNAL(started()), this, SIGNAL(sig_StartTimer()));
    this->prop_Timer->moveToThread(this);
}

void class_Thread::slot_TimeOut()
{
    if(this->prop_Quit){
        this->prop_Timer->stop();
        this->prop_Timer->deleteLater();
        return;
    }
    if(this->prop_Pause){
       // qDebug() << "PAUSE";
        this->msleep(this->prop_BlockTime);
    }
}

void class_Thread::slot_ObjectDestroyed()
{
    // qDebug() << "[DESTROY]";
    emit this->sig_ObjectDestroyed(this);
}
