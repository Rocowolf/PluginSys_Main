#include "core_serverplugin.h"

Core_ServerPlugin::Core_ServerPlugin()
{
    this->prop_Info.insert("plugin_name","core_ServerPlugin");
    this->prop_Info.insert("plugin_version","1.0.D");
    this->prop_Info.insert("plugin_dependence", QJsonArray());
    QJsonObject m_qml;
    m_qml.insert("qml_name","远程服务");
    m_qml.insert("qml_qmlurl","qrc:/core_ServerPlugin/MyItem.qml");
    m_qml.insert("qml_pluginname",this->prop_Info["plugin_name"]);
    this->prop_Info.insert("plugin_qml", m_qml);
}

Core_ServerPlugin::~Core_ServerPlugin()
{
    this->prop_AboutToClose = true;
    foreach(auto m_item, this->prop_SpareThreadList){
        m_item->terminate();
    }
    foreach(auto m_item, this->prop_BusyThreadList){
        m_item->terminate();
    }
}

QJsonObject Core_ServerPlugin::vfunc_GetInformation()
{
    return this->prop_Info;
}

int Core_ServerPlugin::vfunc_Run()
{
    qDebug() << "[core_ServerPlugin] Run";
    foreach(auto m_item, this->prop_BusyThreadList){
        m_item->start();
        this->prop_SpareThreadList.append(m_item);
    }
    return 0;
}

int Core_ServerPlugin::vfunc_Exit()
{
    qDebug() << "[core_ServerPlugin] Exit";
    this->prop_AboutToClose = true;
    foreach(auto m_item, this->prop_BusyThreadList){
        m_item->terminate();
        m_item->deleteLater();
    }
    foreach(auto m_item, this->prop_SpareThreadList){
        m_item->terminate();
        m_item->deleteLater();
    }
    return 0;
}

void Core_ServerPlugin::vfunc_Debug()
{
    qDebug().noquote() << "==========================================================";
    qDebug().noquote() << tr("[%1] DEBUG").arg(this->prop_Info["plugin_name"].toString());
    qDebug().noquote() << tr("插件版本: %1").arg(this->prop_Info["plugin_version"].toString());
    qDebug().noquote() << tr("插件依赖项:");
    QJsonArray m_arr = this->prop_Info["plugin_dependence"].toArray();
    if(m_arr.isEmpty()){
        qDebug().noquote() << tr("\t无");
    }
    else{
        foreach(auto m_item, m_arr){
            qDebug().noquote() << tr("\t%1").arg(m_item.toString());
        }
    }
    qDebug().noquote() << tr("插件QML信息:");
    if(this->prop_Info["plugin_qml"].toObject().isEmpty()){
        qDebug().noquote() << tr("\t无");
    }
    else{
        QJsonObject m_obj = this->prop_Info["plugin_qml"].toObject();
        for(auto m_item = m_obj.begin(); m_item != m_obj.end(); ++m_item){
            qDebug().noquote() << "\t" << m_item.key() << ":" << m_item.value().toString();
        }
    }
    qDebug().noquote() << "==========================================================";
}

bool Core_ServerPlugin::vfunc_Vertify(QPluginLoader* p_loader)
{
    qDebug() << "[core_ServerPlugin] Vertify";
    auto m_conf = QFile("../config/core_ServerPlugin.conf");
    if(!m_conf.exists() || !m_conf.open(QIODevice::ReadOnly)){
        qDebug() << "[core_ServerPlugin] Error Open";
        return false;
    }
    auto m_info = QJsonDocument::fromJson(m_conf.readAll()).object();
    if(!m_info.contains("conf_downloadpath") || !m_info.contains("conf_serverip") || !m_info.contains("conf_serverport")){
        return false;
    }
    this->prop_ServerIp = QHostAddress(m_info.value("conf_serverip").toString());
    this->prop_ServerPort = m_info.value("conf_serverport").toInt();
    this->prop_LocalPath = m_info.value("conf_downloadpath").toString();
    m_conf.close();

    if(qobject_cast<Interface_ServerPlugin*>(p_loader->instance()) != nullptr){
        connect(this, &Core_ServerPlugin::vsig_Download, this, [this](QString p_name){
            if(p_name == "ls"){
                this->func_Download("ls", false);
            }
            else{
                this->func_Download(p_name, true);
            }
        });

        qmlRegisterSingletonInstance<ServerModel>("ServerPlugin_ServerModel", 1, 0, "ServerPlugin_ServerModel", ServerModel::sfunc_GetInstance(this->prop_ServerIp.toString(), this->prop_LocalPath));
        this->prop_ServerModel = ServerModel::sfunc_GetInstance("","");
        connect(this->prop_ServerModel, SIGNAL(sig_AboutToDownload(QString)), this, SIGNAL(vsig_Download(QString)));
        connect(this->prop_ServerModel, SIGNAL(sig_AboutToAbort(QString)), this, SIGNAL(vsig_Abort(QString)));
        connect(this->prop_ServerModel, SIGNAL(sig_AboutToInstall(QString)), this, SIGNAL(vsig_Install(QString)));
        connect(this->prop_ServerModel, SIGNAL(sig_AboutToUninstall(QString)), this, SIGNAL(vsig_Uninstall(QString)));
        connect(this, SIGNAL(vsig_Buffered(QString,QByteArray)), this->prop_ServerModel, SLOT(slot_Data(QString,QByteArray)));
        connect(this, SIGNAL(vsig_Downloaded(QString)), this->prop_ServerModel, SIGNAL(sig_Downloaded(QString)));
        connect(this, SIGNAL(vsig_Update(QString,qint64,qint64)), this->prop_ServerModel, SIGNAL(sig_Updated(QString,qint64,qint64)));
        connect(this, SIGNAL(vsig_Installed(QString,bool)), this->prop_ServerModel, SIGNAL(sig_Installed(QString,bool)));
        connect(this, SIGNAL(vsig_Uninstalled(QString,bool)), this->prop_ServerModel, SIGNAL(sig_Uninstalled(QString,bool)));

        auto m_thread = new class_Thread;
        connect(m_thread, SIGNAL(sig_ObjectDestroyed(class_Thread*)), this, SLOT(slot_TaskFinished(class_Thread*)));
        this->prop_BusyThreadList.append(m_thread);
        m_thread = new class_Thread;
        connect(m_thread, SIGNAL(sig_ObjectDestroyed(class_Thread*)), this, SLOT(slot_TaskFinished(class_Thread*)));
        this->prop_BusyThreadList.append(m_thread);
        m_thread = new class_Thread;
        connect(m_thread, SIGNAL(sig_ObjectDestroyed(class_Thread*)), this, SLOT(slot_TaskFinished(class_Thread*)));
        this->prop_BusyThreadList.append(m_thread);
        return true;
    }
    else{
        return false;
    }
}

void Core_ServerPlugin::vfunc_Init(const QMap<QString, QSharedPointer<QPluginLoader> >* p_pluginmap, const QStringList* p_filelist)
{
    this->prop_ServerModel->func_Init(p_pluginmap, p_filelist);
}

bool Core_ServerPlugin::func_Download(QString p_name, bool p_isfile)
{
    if(this->prop_AboutToClose){
        return false;
    }
    if(!this->prop_SpareThreadList.isEmpty() && this->prop_TaskList.isEmpty()){
        // qDebug() << "[DEBUG] " << p_name;
        auto m_task = new class_Task;
        auto m_thread = this->prop_SpareThreadList.takeFirst();
        emit m_thread->sig_StopTimer();
        m_thread->prop_Pause = false;
        connect(this, SIGNAL(sig_Run(class_Thread*)), m_task, SLOT(slot_Run(class_Thread*)));
        connect(this, SIGNAL(vsig_Abort(QString)), m_task, SLOT(slot_Abort(QString)));
        // connect(m_task, SIGNAL(destroyed(QObject*)), this, SLOT(slot_Debug()));
        connect(m_task, SIGNAL(sig_Finished(QString,QByteArray)), this, SIGNAL(vsig_Buffered(QString,QByteArray)));
        connect(m_task, SIGNAL(sig_Aborted(QString)), this, SIGNAL(vsig_Aborted(QString)));
        connect(m_task, SIGNAL(sig_Update(QString,qint64,qint64)), this, SIGNAL(vsig_Update(QString,qint64,qint64)));
        connect(m_task, SIGNAL(destroyed(QObject*)), m_thread, SLOT(slot_ObjectDestroyed()));
        connect(m_task, &class_Task::destroyed, this, [p_name,this](){
            emit this->vsig_Downloaded(p_name);
        });

        this->prop_BusyThreadList.append(m_thread);
        QJsonObject m_obj;
        m_obj.insert("task_sourcename",p_name);
        m_obj.insert("task_localpath", this->prop_LocalPath);
        auto m_ret = m_task->func_Init(this->prop_ServerIp, this->prop_ServerPort, p_isfile ? class_Task::E_File : class_Task::E_Data, m_obj);
        m_task->moveToThread(m_thread);
        emit this->sig_Run(m_thread);
        return m_ret;
    }
    if(this->prop_SpareThreadList.isEmpty()){
        // qDebug() << "[DEBUG] " << "[ADD]";
        auto m_task = new class_Task;
        QJsonObject m_obj;
        m_obj.insert("task_sourcename",p_name);
        m_obj.insert("task_localpath", this->prop_LocalPath);
        auto m_ret = m_task->func_Init(this->prop_ServerIp, this->prop_ServerPort, p_isfile ? class_Task::E_File : class_Task::E_Data, m_obj);
        this->prop_TaskList.append(m_task);
        return m_ret;
    }
    return false;
}

void Core_ServerPlugin::slot_TaskFinished(class_Thread* p_workthread)
{
    if(this->prop_AboutToClose){
        return;
    }
    if(this->prop_TaskList.isEmpty()){
        p_workthread->prop_Pause = true;
        emit p_workthread->sig_StartTimer();
        // qDebug() << "Back To SpareList";
        this->prop_BusyThreadList.removeOne(p_workthread);
        this->prop_SpareThreadList.append(p_workthread);
    }
    else{
        auto m_task = this->prop_TaskList.takeFirst();
        auto m_name = m_task->prop_SourceName;
        connect(this, SIGNAL(sig_Run(class_Thread*)), m_task, SLOT(slot_Run(class_Thread*)));
        connect(this, SIGNAL(vsig_Abort(QString)), m_task, SLOT(slot_Abort(QString)));
        // connect(m_task, SIGNAL(destroyed(QObject*)), this, SLOT(slot_Debug()));
        connect(m_task, SIGNAL(sig_Finished(QString,QByteArray)), this, SIGNAL(vsig_Buffered(QString,QByteArray)));
        connect(m_task, SIGNAL(sig_Aborted(QString)), this, SIGNAL(vsig_Aborted(QString)));
        connect(m_task, SIGNAL(sig_Update(QString,qint64,qint64)), this, SIGNAL(vsig_Update(QString,qint64,qint64)));
        connect(m_task, SIGNAL(destroyed(QObject*)), p_workthread, SLOT(slot_ObjectDestroyed()));
        connect(m_task, &class_Task::destroyed, p_workthread, [m_name,this](){
            emit this->vsig_Downloaded(m_name);
        });
        m_task->moveToThread(p_workthread);
        emit this->sig_Run(p_workthread);
    }
}

void Core_ServerPlugin::slot_Debug()
{
    // qDebug() << "Object Deleted";
}
