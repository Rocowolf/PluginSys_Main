import QtQuick 2.15
import QtQuick.Controls
import ServerPlugin_ServerModel 1.0

Rectangle {
    id: root

    width: 640
    height: 480
    visible: true

    Column {
        id: column0
        padding: 10
        spacing: 10

        Row {
            id: row0
            spacing: 10

            TextField{
                id: textfield0
                width: contentWidth + leftPadding*2
                height: 30
                text: ServerPlugin_ServerModel.prop_IP
                verticalAlignment: TextField.AlignVCenter
                font.pixelSize: height*0.5
                leftPadding: 10
                rightPadding: leftPadding
                readOnly: true
            }

            Text{
                id: text0
                text: "|"
                height: 30
                font.pixelSize: height*0.5
                verticalAlignment: TextField.AlignVCenter
            }

            TextField{
                width: Math.min(contentWidth + leftPadding*2 , root.width - 2*column0.padding - textfield0.width - text0.width - 2*row0.spacing)
                height: 30
                text: ServerPlugin_ServerModel.prop_Path
                verticalAlignment: TextField.AlignVCenter
                font.pixelSize: height*0.5
                leftPadding: 10
                rightPadding: leftPadding
                readOnly: true
            }
        }

        Rectangle {
            width: root.width - 2*column0.padding
            height: root.height - 2*column0.padding - column0.spacing - row0.implicitHeight
            border.width: 2

            ListView{
                id: listview0
                property bool prop_Refresh: false
                width: parent.width - 10
                height: parent.height - 10
                anchors.centerIn: parent
                spacing: 5
                clip: true
                enabled: true

                onContentYChanged: {
                    if(listview0.prop_Refresh){
                        return;
                    }
                    if(contentY < -150){
                        listview0.prop_Refresh = true
                    }
                }

                onMovementEnded: {
                    if(listview0.prop_Refresh){
                        mask.visible = true
                        listview0.enabled = false
                        listview0.prop_Refresh = false
                        ServerPlugin_ServerModel.sig_AboutToDownload("ls")
                    }
                }

                Connections{
                    target: ServerPlugin_ServerModel
                    function onSig_Refreshed(){
                        mask.visible = false
                        listview0.enabled = true
                    }
                }

                Rectangle{
                    id: mask2
                    width: parent.width
                    height: Math.max(-listview0.contentY,0)
                    color: "gray"

                    Text{
                        visible: parent.height >= 150
                        text: listview0.prop_Refresh ? "松开以刷新列表" : ""
                        anchors.centerIn: parent
                        width: parent.width * 0.7
                        height: 50
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: height
                        fontSizeMode: Text.Fit
                    }
                }

                model: ServerPlugin_ServerModel
                delegate: Rectangle {
                    id: outline0
                    property int prop_Persentage: 0
                    width: listview0.width
                    height: 50
                    border.width: 1

                    Row{
                        id: row1
                        padding: 5
                        spacing: 5
                        anchors.fill: parent

                        Text{
                            text: plugin_name
                            height: outline0.height - 2*row1.padding
                            width: row1.width -2*row1.padding - row1.spacing - button1.width
                            fontSizeMode: Text.Fit
                            font.pixelSize: height
                            verticalAlignment: Text.AlignVCenter
                        }

                        Button{
                            id: button1
                            //0---下载
                            //1---安装
                            //2---卸载
                            property int prop_Mode: 0
                            height: outline0.height - 2*row1.padding
                            width: height*2
                            text: "下载"
//                                      "下载"
                            Component.onCompleted: {
                                switch(plugin_state){
                                case 0:
                                    button1.text = "下载";
                                    button1.prop_Mode = 0;
                                    break;
                                case 1:
                                    button1.text = "安装";
                                    button1.prop_Mode = 1;
                                    break;
//                                case 2:
//                                    button1.text = "卸载";
//                                    button1.prop_Mode = 2;
//                                    break;
//                                }
                                case 2:
                                    button1.text = "已安装";
                                    button1.prop_Mode = 2;
                                    button1.enabled = false;
                                    break;
                                }
                            }

                            Connections{
                                target: ServerPlugin_ServerModel
                                function onSig_Updated(p_name, p_cur, p_total){
                                    if(p_name === plugin_name){
                                        button1.text = Math.ceil(p_cur/p_total * 100).toString() + "%"
                                    }
                                }
                                function onSig_Downloaded(p_name){
                                    if(p_name === plugin_name){
                                        button1.text = "安装";
                                        button1.prop_Mode = 1
                                        button1.enabled = true
                                    }
                                }
//                                function onSig_Installed(p_name,p_state){
//                                    if(p_name === plugin_name){
//                                        button1.text = p_state ? "卸载" : "重新安装";
//                                        button1.prop_Mode = p_state ? 2 : 1
//                                        button1.enabled = true
//                                    }
//                                }
                                function onSig_Installed(p_name,p_state){
                                    if(p_name === plugin_name){
                                        button1.text = p_state ? "已安装" : "重新安装";
                                        button1.prop_Mode = p_state ? 2 : 1
                                        button1.enabled = p_state ? false : true
                                    }
                                }
//                                function onSig_Uninstalled(p_name, p_state){
//                                    if(p_name === plugin_name){
//                                        button1.text = p_state ? "下载" : "重新卸载";
//                                        button1.prop_Mode = p_state ? 0 : 2
//                                        button1.enabled = true
//                                    }
//                                }
                            }

                            onClicked: {
                                switch(button1.prop_Mode){
                                case 0:
                                    button1.enabled = false
                                    button1.text = "0%"
                                    ServerPlugin_ServerModel.sig_AboutToDownload(plugin_name)
                                    break;

                                case 1:
                                    button1.enabled = false
                                    button1.text = "安装中"
                                    ServerPlugin_ServerModel.sig_AboutToInstall(plugin_name)
                                    break;

//                                case 2:
//                                    button1.enabled = false;
//                                    button1.text = "卸载中"
//                                    ServerPlugin_ServerModel.sig_AboutToUninstall(plugin_name)
//                                    break;
                                }
                            }
                        }
                    }
                }

                Rectangle {
                    id: mask
                    anchors.fill: parent
                    opacity: 0.8
                    visible: false
                }
            }
        }
    }
}
