#include "servermodel.h"

ServerModel::ServerModel(const QString& p_ip, const QString& p_path, QObject *parent)
    : QAbstractListModel(parent)
{
    this->prop_HashMap.insert(0, "plugin_name");
    this->prop_HashMap.insert(1, "plugin_version");
    this->prop_HashMap.insert(2, "plugin_state");
    this->prop_HashMap.insert(3, "plugin_size");
    this->prop_HashMap.insert(4, "plugin_current");

    this->prop_IP = p_ip;
    this->prop_Path = p_path;
}

ServerModel *ServerModel::sfunc_GetInstance(const QString& p_ip, const QString& p_path)
{
    if(ServerModel::sprop_Instance == nullptr){
        ServerModel::sprop_Instance = new ServerModel(p_ip, p_path);
    }
    return ServerModel::sprop_Instance;
}

int ServerModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
    return this->prop_PluginList.count();
}

QHash<int, QByteArray> ServerModel::roleNames() const
{
    return this->prop_HashMap;
}

QVariant ServerModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    return this->prop_PluginList[index.row()].value(this->prop_HashMap[role]);
}

void ServerModel::func_Init(const QMap<QString, QSharedPointer<QPluginLoader>>* p_pluginmap, const QStringList* p_filelist)
{
    this->prop_LoaderMap = p_pluginmap;
    this->prop_FileList = p_filelist;
}

QString ServerModel::getProp_Path() const
{
    return prop_Path;
}

void ServerModel::setProp_Path(const QString &newProp_Path)
{
    if (prop_Path == newProp_Path)
        return;
    prop_Path = newProp_Path;
    emit prop_PathChanged();
}

QString ServerModel::getProp_IP() const
{
    return prop_IP;
}

void ServerModel::setProp_IP(const QString &newProp_IP)
{
    if (prop_IP == newProp_IP)
        return;
    prop_IP = newProp_IP;
    emit prop_IPChanged();
}

void ServerModel::func_AboutToDownload(QString p_name)
{
    emit sig_AboutToDownload(p_name);
}

void ServerModel::func_AboutToAbort(QString p_name)
{
    emit sig_AboutToAbort(p_name);
}

void ServerModel::slot_Data(QString p_name, QByteArray p_data)
{
    if(p_name != "ls"){
        return;
    }
    QList<QString> m_pluginlist = this->prop_LoaderMap->keys();
    QStringList m_filelist = *this->prop_FileList;
    // qDebug() << "[core_ServerPlugin] m_pluginlist = " << m_pluginlist;
    // qDebug() << "[core_ServerPlugin] m_filelist = " << m_filelist;
    this->beginResetModel();
    this->prop_PluginList.clear();
    foreach(auto m_item, QJsonDocument::fromJson(p_data).array()){
        QJsonObject m_obj;
        QString m_name = m_item.toObject().value("source_name").toString();
        m_obj.insert("plugin_name",m_name);
        if(m_pluginlist.contains(m_name)){
            m_obj.insert("plugin_state", 2);
        }
        else if(m_filelist.contains(m_name)){
            m_obj.insert("plugin_state", 1);
        }
        else{
            m_obj.insert("plugin_state", 0);
        }
        this->prop_PluginList.append(m_obj);
    }
    this->endResetModel();
    emit this->sig_EndRefreshServer();
    emit this->sig_Refreshed();
}

ServerModel* ServerModel::sprop_Instance = nullptr;
