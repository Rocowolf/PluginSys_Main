#ifndef SERVERMODEL_H
#define SERVERMODEL_H

#include <QAbstractListModel>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QPluginLoader>

class ServerModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ServerModel(const QString& p_ip = "255.255.255.255", const QString& p_path = "/", QObject *parent = nullptr);
    static ServerModel* sfunc_GetInstance(const QString& p_ip, const QString& p_path);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QHash<int,QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    void func_Init(const QMap<QString, QSharedPointer<QPluginLoader>>* p_pluginmap, const QStringList* p_filelist);

    QString prop_IP;
    QString prop_Path;

    Q_INVOKABLE void func_AboutToDownload(QString p_name);
    Q_INVOKABLE void func_AboutToAbort(QString p_name);

    QString getProp_IP() const;
    void setProp_IP(const QString &newProp_IP);

    QString getProp_Path() const;
    void setProp_Path(const QString &newProp_Path);

signals:
    //主动发送信号
    void sig_AboutToRefreshServer();
    void sig_AboutToConnect();
    void sig_AboutToDisconnect();
    void sig_AboutToDownload(QString p_name);
    void sig_AboutToAbort(QString p_name);
    void sig_AboutToInstall(QString p_name);
    void sig_AboutToUninstall(QString p_name);
    void sig_EndRefreshServer();

    //QML接收信号
    void sig_Updated(QString,qint64,qint64);
    void sig_Downloaded(QString);
    void sig_Installed(QString,bool);
    void sig_Uninstalled(QString,bool);
    void sig_Refreshed();

    void prop_IPChanged();

    void prop_PathChanged();

public slots:
    void slot_Data(QString p_name, QByteArray p_data);

private:
    static ServerModel* sprop_Instance;
    QHash<int,QByteArray> prop_HashMap;
    QList<QJsonObject> prop_PluginList;
    const QMap<QString,QSharedPointer<QPluginLoader>>* prop_LoaderMap;
    const QStringList* prop_FileList;
    Q_PROPERTY(QString prop_IP READ getProp_IP WRITE setProp_IP NOTIFY prop_IPChanged FINAL)
    Q_PROPERTY(QString prop_Path READ getProp_Path WRITE setProp_Path NOTIFY prop_PathChanged FINAL)
};

#endif // SERVERMODEL_H
