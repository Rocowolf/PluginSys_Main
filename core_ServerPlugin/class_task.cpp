#include "class_task.h"

class_Task::class_Task(QObject *parent)
    : QObject{parent}
{

}

bool class_Task::func_Init(const QHostAddress& p_ip, const quint16 p_port,
                           enum_TaskType p_type, const QJsonObject& p_task)
{
    this->prop_SourceName = p_task.value("task_sourcename").toString("");
    if(this->prop_SourceName == ""){
        return false;
    }
    this->prop_TaskType = p_type;
    if(this->prop_SourceName == "ls"){
        this->prop_TaskType = E_Data;
    }
    switch(this->prop_TaskType){
    case E_Data:
        break;
    case E_File:
        this->prop_TargetPath = p_task.value("task_localpath").toString("");
        if(this->prop_SourceName == ""){
            return false;
        }
    }
    this->prop_Ip = p_ip;
    this->prop_Port = p_port;
    return true;
}

void class_Task::slot_Abort(QString p_name)
{
    if(p_name == this->prop_SourceName || p_name == ""){
        if(this->prop_TaskStatus != E_Quit){
            this->prop_TaskStatus = E_Abort;
        }
        if(this->prop_Socket != nullptr){
            this->prop_Socket->abort();
            this->prop_Socket->deleteLater();
        }
        if(this->prop_Target != nullptr && this->prop_Target->isOpen()){
            this->prop_Target->close();
            this->prop_Target->remove();
            this->prop_Target->deleteLater();
        }
        if(this->prop_TaskStatus != E_Quit){
            this->deleteLater();
            // qDebug() << "[TASK] Aborted";
            emit this->sig_Aborted(this->prop_SourceName);
//            emit this->sig_NextTask(this->prop_WorkThread);
        }
        else{
            this->deleteLater();
            // qDebug() << "[TASK] Aborted";
            emit this->sig_Aborted(this->prop_SourceName);
//            emit this->sig_NextTask(this->prop_WorkThread);
//            this->prop_WorkThread->quit();
        }
    }
}

void class_Task::slot_Finished()
{
    if(this->prop_TaskStatus == E_Finished){
        this->prop_Socket->disconnectFromHost();
        this->prop_Socket->deleteLater();
        this->deleteLater();
    }
}

void class_Task::slot_Connected()
{
    if(this->prop_TaskStatus == E_Default){
        this->prop_TaskStatus = E_Connect;
        QJsonObject m_ret;
        m_ret.insert("client_confirm",true);
        m_ret.insert("source_name", this->prop_SourceName);
        this->prop_Socket->write(QJsonDocument(m_ret).toJson());
    }
}

// void class_Task::slot_Disconnected()
// {
//     if(this->prop_TaskStatus == E_Finished){
// //        emit this->sig_NextTask(this->prop_WorkThread);
//         this->deleteLater();
//         return;
//     }
//     if(this->prop_TaskStatus == E_Abort || this->prop_TaskStatus == E_Quit){
//         return;
//     }
//     // qDebug() << "[TASK] Aborted";
//     emit this->sig_Aborted(this->prop_SourceName);
//     this->deleteLater();
// }

void class_Task::slot_Disconnected()
{
    if(this->prop_TaskStatus == E_Finished){
        //        emit this->sig_NextTask(this->prop_WorkThread);
        this->deleteLater();
        return;
    }
    if(this->prop_TaskStatus == E_Abort || this->prop_TaskStatus == E_Quit){
        return;
    }
    qDebug() << "[core_ServerPlugin] 传输被远程终止";
    if(this->prop_Target != nullptr){
        this->prop_Target->close();
        this->prop_Target->remove();
        this->prop_Target->deleteLater();
    }
    emit this->sig_Aborted(this->prop_SourceName);
    this->deleteLater();
}

void class_Task::slot_ReadyRead()
{
    if(this->prop_TaskStatus == E_Connect){
        auto m_data = this->prop_Socket->readAll();
        if(this->prop_SourceSize == 0 && this->prop_TaskType != E_Data){
            auto m_json = QJsonDocument::fromJson(m_data).object();

            int m_jval = m_json.value("source_state").toInt(-1);
            if(m_jval != 0){
                this->slot_Abort(this->prop_SourceName);
            }
            if(this->prop_SourceName != m_json.value("source_name").toString("")){
                this->slot_Abort(this->prop_SourceName);
            }
            if((this->prop_SourceSize = m_json.value("source_size").toInteger()) == 0){
                this->slot_Abort(this->prop_SourceName);
            }
            QJsonObject m_ret;
            m_ret.insert("client_state","ok");
            m_ret.insert("source_name",m_json.value("source_name").toString(""));
            m_ret.insert("client_confirm",true);
            this->prop_Socket->write(QJsonDocument(m_ret).toJson());
        }
        else if(this->prop_TaskType == E_Data){
            this->prop_Buffer.append(m_data);
            this->prop_CurrentSize += m_data.size();
            // if(this->prop_CurrentSize == this->prop_SourceSize){
            //     this->prop_TaskStatus = E_Finished;
            //     emit this->sig_Update(this->prop_SourceName, this->prop_CurrentSize, this->prop_SourceSize);
            //     emit this->sig_Finished(this->prop_SourceName, this->prop_Buffer);
            //     this->prop_Socket->disconnectFromHost();
            //     this->prop_Socket->deleteLater();
            //     this->deleteLater();
            // }
            if(QJsonDocument::fromJson(this->prop_Buffer).isArray()){
                // qDebug() << this->prop_TargetPath << " 下载完成";
                qDebug() << "[core_ServerPlugin] 数据下载完成";
                this->prop_TaskStatus = E_Finished;
                emit this->sig_Finished(this->prop_SourceName, this->prop_Buffer);
                this->prop_Socket->abort();
                this->prop_Socket->deleteLater();
                this->deleteLater();
            }
            // else{
            //     emit this->sig_Update(this->prop_SourceName, this->prop_CurrentSize, this->prop_SourceSize);
            // }
        }
        else if(this->prop_TaskType == E_File){
            this->prop_Target->write(m_data);
            this->prop_CurrentSize += m_data.size();
            // qDebug() << "Read (" << this->prop_CurrentSize << "/" << this->prop_SourceSize << ")";
            if(this->prop_CurrentSize == this->prop_SourceSize){
                this->prop_TaskStatus = E_Finished;
                this->prop_Target->close();
                qDebug() << "[core_ServerPlugin] " << this->prop_TargetPath << " 下载完成";
                emit this->sig_Update(this->prop_SourceName, this->prop_CurrentSize, this->prop_SourceSize);
                emit this->sig_Finished(this->prop_SourceName, QByteArray());
                this->prop_Socket->disconnectFromHost();
                this->prop_Socket->deleteLater();
                this->deleteLater();
            }
            else{
                emit this->sig_Update(this->prop_SourceName, this->prop_CurrentSize, this->prop_SourceSize);
            }
        }
    }
}

// void class_Task::slot_Run(class_Thread* p_workthread)
// {
//     if(this->prop_TaskStatus != E_Default){
//         return;
//     }
//     if(this->prop_WorkThread != nullptr){
//         return;
//     }
//     else{
//         if(p_workthread != nullptr){
//             this->prop_WorkThread = p_workthread;
//             this->prop_Target = new QFile(this->prop_TargetPath.append(this->prop_SourceName),this);
//             if(!this->prop_Target->open(QIODevice::ReadWrite | QIODevice::Truncate)){
//                 this->slot_Abort(this->prop_SourceName);
//             }
//             this->prop_Socket = new QTcpSocket(this);
//             connect(this->prop_Socket, SIGNAL(connected()), this, SLOT(slot_Connected()));
//             connect(this->prop_Socket, SIGNAL(disconnected()), this, SLOT(slot_Disconnected()));
//             connect(this->prop_Socket, SIGNAL(readyRead()), this, SLOT(slot_ReadyRead()));
//             this->prop_Socket->connectToHost(this->prop_Ip, this->prop_Port);
//             if(!this->prop_Socket->waitForConnected()){
//                 this->slot_Abort(this->prop_SourceName);
//             }
//         }
//         else{
//             this->slot_Abort(this->prop_SourceName);
//         }
//     }
// }

void class_Task::slot_Run(class_Thread* p_workthread)
{
    if(this->prop_TaskStatus != E_Default){
        return;
    }
    if(this->prop_WorkThread != nullptr){
        return;
    }
    else{
        if(p_workthread != nullptr){
            this->prop_WorkThread = p_workthread;
            if(this->prop_TaskType == E_File){
                this->prop_Target = new QFile(this->prop_TargetPath.append(this->prop_SourceName),this);
                if(!this->prop_Target->open(QIODevice::ReadWrite | QIODevice::Truncate)){
                    this->slot_Abort(this->prop_SourceName);
                }
            }
            this->prop_Socket = new QTcpSocket(this);
            connect(this->prop_Socket, SIGNAL(connected()), this, SLOT(slot_Connected()));
            connect(this->prop_Socket, SIGNAL(disconnected()), this, SLOT(slot_Disconnected()));
            connect(this->prop_Socket, SIGNAL(readyRead()), this, SLOT(slot_ReadyRead()));
            this->prop_Socket->connectToHost(this->prop_Ip, this->prop_Port);
            if(!this->prop_Socket->waitForConnected()){
                this->slot_Abort(this->prop_SourceName);
            }
        }
        else{
            this->slot_Abort(this->prop_SourceName);
        }
    }
}
