#ifndef CLASS_THREAD_H
#define CLASS_THREAD_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QTimer>

class class_Thread : public QThread
{
    Q_OBJECT
public:
    explicit class_Thread(QObject *parent = nullptr);

    QTimer* prop_Timer = nullptr;
    bool prop_Quit = false;
    bool prop_Pause = true;
    int prop_BlockTime = 100;

signals:
    void sig_ObjectDestroyed(class_Thread* p_self);
    void sig_StartTimer();
    void sig_StopTimer();

public slots:
    void slot_TimeOut();
    void slot_ObjectDestroyed();

private:
//    QTimer* prop_Timer = nullptr;
};

#endif // CLASS_THREAD_H
