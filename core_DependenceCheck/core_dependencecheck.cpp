#include "core_dependencecheck.h"

Core_DependenceCheck::Core_DependenceCheck()
{
    this->prop_Info.insert("plugin_name","core_DependenceCheck");
    this->prop_Info.insert("plugin_version","1.0.D");
}

QJsonObject Core_DependenceCheck::vfunc_GetInformation()
{
    return this->prop_Info;
}

int Core_DependenceCheck::vfunc_Run()
{
    qDebug() << "[core_DependenceCheck] Run";
    return 0;
}

int Core_DependenceCheck::vfunc_Exit()
{
    qDebug() << "[core_DependenceCheck] Exit";
    return 0;
}

void Core_DependenceCheck::vfunc_Debug()
{
    qDebug().noquote() << "==========================================================";
    qDebug().noquote() << tr("[%1] DEBUG").arg(this->prop_Info["plugin_name"].toString());
    qDebug().noquote() << tr("插件版本: %1").arg(this->prop_Info["plugin_version"].toString());
    qDebug().noquote() << tr("插件依赖项:");
    QJsonArray m_arr = this->prop_Info["plugin_dependence"].toArray();
    if(m_arr.isEmpty()){
        qDebug().noquote() << tr("\t无");
    }
    else{
        foreach(auto m_item, m_arr){
            qDebug().noquote() << tr("\t%1").arg(m_item.toString());
        }
    }
    qDebug().noquote() << tr("插件QML信息:");
    if(this->prop_Info["plugin_qml"].toObject().isEmpty()){
        qDebug().noquote() << tr("\t无");
    }
    else{
        QJsonObject m_obj = this->prop_Info["plugin_qml"].toObject();
        for(auto m_item = m_obj.begin(); m_item != m_obj.end(); ++m_item){
            qDebug().noquote() << "\t" << m_item.key() << ":" << m_item.value().toString();
        }
    }
    qDebug().noquote() << "==========================================================";
}

bool Core_DependenceCheck::vfunc_Vertify(QPluginLoader* p_loader)
{
    return (qobject_cast<Interface_DependenceCheck*>(p_loader->instance()) != nullptr);
}

QJsonValue Core_DependenceCheck::vfunc_LoadPlugin(QJsonObject m_obj)
{
    if(m_obj.isEmpty()){
        return QJsonObject();
    }

    QJsonArray m_loaded;
    QJsonArray m_notloaded;
    // QJsonArray m_reload;
    QJsonObject m_ret;

    int row = 0;

    // foreach(auto m_item, m_obj.keys()){
    //     if(this->prop_PluginList.remove(m_item)){
    //         m_reload.append(m_item);
    //     }
    // }

    while(!m_obj.isEmpty() && row < 30){
        row++;

        for(auto m_item = m_obj.begin(); m_item != m_obj.end(); ++m_item){
            //   false:跳过    true:检查完成
            bool m_state = true;

            QJsonArray m_arr = m_item.value().toArray();//dependence arr
            if(m_arr.isEmpty()){
                m_loaded.append(m_item.key());
                this->prop_PluginList.insert(m_item.key());
            }
            for(auto m_item2 = m_arr.begin(); m_item2 != m_arr.end(); ++m_item2){

                //如果待检测队列含有依赖项则先不检查该插件
                if(m_obj.contains(m_item2->toString())){
                    m_state = false;
                    break;
                }

                //如果待检测队列不含有依赖项 且 依赖项检测通过列表也不含依赖项 则认为该插件缺少依赖项
                else if(!this->prop_PluginList.contains(m_item2->toString())){
                    m_notloaded.append(m_item.key());
                    m_state = true;
                    break;
                }
                else{
                    m_loaded.append(m_item.key());
                    m_state = true;
                    this->prop_PluginList.insert(m_item.key());
                }
            }
            if(!m_state){
                continue;
            }
            else{
                m_obj.erase(m_item);
                if(m_item == m_obj.end()){
                    break;
                }
            }
        }
    }

    if(!m_obj.isEmpty() && row == 30){
        qDebug() << "[core_DependenceCheck] 插件验证迭代超上限";
        m_ret.insert("pass",QJsonArray::fromStringList(m_obj.keys()));
    }

    m_ret.insert("failed",m_notloaded);
    m_ret.insert("success",m_loaded);

    return m_ret;
}

void Core_DependenceCheck::vfunc_UnloadPlugin(QString p_name)
{
    this->prop_PluginList.remove(p_name);
}
