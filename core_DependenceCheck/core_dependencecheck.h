#ifndef CORE_DEPENDENCECHECK_H
#define CORE_DEPENDENCECHECK_H

#include "core_DependenceCheck_global.h"
#include "../interface/interface_DependenceCheck.h"

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

class CORE_DEPENDENCECHECK_EXPORT Core_DependenceCheck : public QObject,Interface_DependenceCheck
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Rocowolf.DependenceCheck")
    Q_INTERFACES(Interface_DependenceCheck)

public:
    Core_DependenceCheck();

    QJsonObject vfunc_GetInformation() override;
    int vfunc_Run() override;
    int vfunc_Exit() override;
    void vfunc_Debug() override;
    bool vfunc_Vertify(QPluginLoader* p_loader) override;

    QJsonValue vfunc_LoadPlugin(QJsonObject m_obj) override;
    void vfunc_UnloadPlugin(QString p_name) override;

signals:
    // void vsig_Installed(QString p_name, bool p_state) override;

public slots:
    // void vslot_UnloadPlugin(QJsonArray p_arr) override;
    // void vslot_Install(QString p_name) override;

private:
    QJsonObject prop_Info;
    QSet<QString> prop_PluginList;
};

#endif // CORE_DEPENDENCECHECK_H
