#ifndef PLUGIN_REMOTECONTROLLER_H
#define PLUGIN_REMOTECONTROLLER_H

#include "plugin_RemoteController_global.h"
#include "../interface/interface_UserBase.h"

#include <QTcpSocket>
#include <QJsonDocument>
#include <QJsonArray>
#include <QFile>
#include <QHostAddress>
#include <QtConcurrent>

/*
    {
        "method":"post",
        "command":"led",
        "param":"1"
    }
*/

class PLUGIN_REMOTECONTROLLER_EXPORT Plugin_RemoteController : public QObject,Interface_UserBase
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Rocowolf.UserPlugin")
    Q_INTERFACES(Interface_UserBase)

public:
    Plugin_RemoteController();

    QJsonObject vfunc_GetInformation() override;
    bool vfunc_Vertify(QPluginLoader* p_loader) override;
    int vfunc_Run() override;
    int vfunc_Exit() override;
    void vfunc_Debug() override;

    void vfunc_Init(const QMap<QString,QSharedPointer<QPluginLoader>>* p_pluginptr) override;

signals:
    void vsig_Unload(QString p_name) override;

    void sig_Write(QByteArray);

public slots:
    void slot_ReadyRead();

private:
    QTcpSocket* prop_Socket = nullptr;
    QJsonObject prop_Config;
    QJsonObject prop_Info;

    QHostAddress prop_RemoteIP;
    quint16 prop_RemotePort;
    QJsonObject prop_CommandList;
};

#endif // PLUGIN_REMOTECONTROLLER_H
