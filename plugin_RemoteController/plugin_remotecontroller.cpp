#include "plugin_remotecontroller.h"

Plugin_RemoteController::Plugin_RemoteController()
{
    this->prop_Info.insert("plugin_name","plugin_RemoteController");
    this->prop_Info.insert("plugin_version","1.0.D");
    this->prop_Info.insert("plugin_dependence", QJsonArray());
    //    QJsonObject m_qml;
    //    m_qml.insert("qml_name","串口模块");
    //    m_qml.insert("qml_qmlurl","qrc:/SerialServer/MyItem.qml");
    //    m_qml.insert("qml_imgurl","qrc:/SerialServer/serial.png");
    //    m_qml.insert("qml_pluginname",this->prop_Info["plugin_name"]);
    //    this->prop_Info.insert("plugin_qml", m_qml);
}

QJsonObject Plugin_RemoteController::vfunc_GetInformation()
{
    return this->prop_Info;
}

bool Plugin_RemoteController::vfunc_Vertify(QPluginLoader* p_loader)
{
    auto m_file = QFile("../config/plugin_RemoteController.conf");
    if(!m_file.open(QIODevice::ReadWrite)){
        qDebug() << "[plugin_RemoteController] 无法打开 ../config/plugin_RemoteController.conf 文件";
        return false;
    }
    this->prop_Config = QJsonDocument::fromJson(m_file.readAll()).object();
    m_file.close();
    if(!this->prop_Config.contains("remoteip") || !this->prop_Config.contains("remoteport")){
        return false;
    }
    return true;
}

int Plugin_RemoteController::vfunc_Run()
{
    qDebug() << "[plugin_RemoteController] Run";
    this->prop_Socket = new QTcpSocket;
    connect(this->prop_Socket, SIGNAL(readyRead()), this, SLOT(slot_ReadyRead()));
    connect(this, &Plugin_RemoteController::sig_Write, this->prop_Socket, [this](QByteArray p_data){
            this->prop_Socket->write(p_data);
        }, Qt::QueuedConnection);
    this->prop_RemoteIP = QHostAddress(this->prop_Config.value("remoteip").toString());
    this->prop_RemotePort = this->prop_Config.value("remoteport").toInt();
    this->prop_CommandList = this->prop_Config.value("commandlist").toObject();
    qDebug() << "[plugin_RemoteController] IP = " << this->prop_RemoteIP;
    qDebug() << "[plugin_RemoteController] Port = " << this->prop_RemotePort;
    qDebug() << "[plugin_RemoteController] CommandList = " << this->prop_CommandList;
    this->prop_Socket->connectToHost(this->prop_RemoteIP, this->prop_RemotePort);
    return 0;
}

int Plugin_RemoteController::vfunc_Exit()
{
    qDebug() << "[plugin_RemoteController] Exit";
    return 0;
}

void Plugin_RemoteController::vfunc_Debug()
{

}

void Plugin_RemoteController::vfunc_Init(const QMap<QString, QSharedPointer<QPluginLoader> >* p_pluginptr)
{

}

void Plugin_RemoteController::slot_ReadyRead()
{
    auto m_data = this->prop_Socket->readAll();
    qDebug() << "[plugin_RemoteController] " << QString(m_data);
    auto i = QtConcurrent::run([m_data,this](){
        QJsonObject m_obj = QJsonDocument::fromJson(m_data).object();
        QString m_method = m_obj.value("method").toString();
        auto m_socket = m_obj.value("socket");
        if(m_method.isEmpty()){
            emit this->sig_Write(QJsonDocument(QJsonObject({{"client_confirm",true},{"socket",m_socket},{"method","ack"},{"error","lose method"}})).toJson());
            return;
        }
        QString m_command = m_obj.value("command").toString();
        if(m_command.isEmpty()){
            emit this->sig_Write(QJsonDocument(QJsonObject({{"client_confirm",true},{"socket",m_socket},{"method","ack"},{"error","lose command"}})).toJson());
            return;
        }
        if(!this->prop_CommandList.contains(m_command)){
            emit this->sig_Write(QJsonDocument(QJsonObject({{"client_confirm",true},{"socket",m_socket},{"method","ack"},{"error","command not found"}})).toJson());
            return;
        }
        QJsonObject m_commandobj = this->prop_CommandList.value(m_command).toObject();
        QString m_param = m_obj.value("param").toString();
        QFile m_file(m_commandobj.value("target").toString());
        if(m_method == "post"){
            if(!m_file.open(QIODevice::WriteOnly) || !m_file.write(m_param.toLocal8Bit())){
                emit this->sig_Write(QJsonDocument(QJsonObject({{"client_confirm",true},{"socket",m_socket},{"method","ack"},{"error","file cant open"}})).toJson());
                return;
            }
            else{
                m_file.flush();
                m_file.close();
            }
            emit this->sig_Write(QJsonDocument(QJsonObject({{"client_confirm",true},{"socket",m_socket},{"method","ack"}})).toJson());
            return;
        }
        else {
            emit this->sig_Write(QJsonDocument(QJsonObject({{"client_confirm",true},{"socket",m_socket},{"method","ack"},{"error","command not supported"}})).toJson());
            return;
        }
    });
}
