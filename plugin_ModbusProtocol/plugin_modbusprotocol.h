#ifndef PLUGIN_MODBUSPROTOCOL_H
#define PLUGIN_MODBUSPROTOCOL_H

#include "plugin_ModbusProtocol_global.h"
#include "../interface/interface_UserBase.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QObject>

class PLUGIN_MODBUSPROTOCOL_EXPORT Plugin_ModbusProtocol : public QObject,Interface_UserBase
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Rocowolf.UserPlugin")
    Q_INTERFACES(Interface_UserBase)

public:
    Plugin_ModbusProtocol();

    QJsonObject vfunc_GetInformation() override;
    int vfunc_Run() override;
    int vfunc_Exit() override;
    void vfunc_Debug() override;
    bool vfunc_Vertify(QPluginLoader* p_loader) override;
    void vfunc_Init(const QMap<QString,QSharedPointer<QPluginLoader>>* p_pluginptr) override;

    static quint16 sfunc_GetCRC16(const QString& p_str);
    static bool sfunc_CheckCRC16(const QByteArray& p_ba);

signals:
    void vsig_Unload(QString p_name) override;

private:
    QJsonObject prop_Info;

};

#endif // PLUGIN_MODBUSPROTOCOL_H
