#ifndef PLUGIN_SERIALSERVER_H
#define PLUGIN_SERIALSERVER_H

#include "plugin_SerialServer_global.h"
#include "../interface/interface_SerialServer.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QObject>
#include <QQmlEngine>
#include <QtConcurrent>

//#include "serialserver_controller.h"

class PLUGIN_SERIALSERVER_EXPORT Plugin_SerialServer : public QObject,Interface_SerialServer
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Rocowolf.UserPlugin")
    Q_INTERFACES(Interface_UserBase)

public:
    Plugin_SerialServer();

    QJsonObject vfunc_GetInformation() override;
    int vfunc_Run() override;
    int vfunc_Exit() override;
    void vfunc_Debug() override;
    bool vfunc_Vertify(QPluginLoader* p_loader) override;
    void vfunc_Init(const QMap<QString,QSharedPointer<QPluginLoader>>* p_pluginptr) override;
    void vfunc_Add(const QString& p_name, QJsonObject(*p_func)(QByteArray)) override;
    void vfunc_Remove(const QString& p_name) override;

    Q_INVOKABLE QList<QString> func_GetAvailableSerial();
    Q_INVOKABLE QList<QString> func_GetAvailableAnalyser();
    Q_INVOKABLE void func_Open();
    Q_INVOKABLE void func_Close();
    Q_INVOKABLE void func_Set(QString p_name, QString p_analyse, int p_baudrate, int p_databit, int p_stopbit, int p_parity);

    bool prop_IsOpen = false;

    bool getProp_IsOpen() const;
    void setProp_IsOpen(bool newProp_IsOpen);

signals:
    void vsig_Unload(QString p_name) override;

    void sig_DataComplete(QString p_raw, QJsonObject p_data);

    void prop_IsOpenChanged();

public slots:
    void slot_ReadyRead();

private:
    QJsonObject prop_Info;
    int prop_QMLIndex;
    QSerialPort* prop_CurrentSerial = nullptr;
    QMap<QString,QJsonObject(*)(QByteArray)> prop_AnalyseList;
    QJsonObject(*prop_CurrentAnalyse)(QByteArray) = nullptr;
//    QFile* prop_TEST = nullptr;
//    QByteArray prop_TEST2;
    Q_PROPERTY(bool prop_IsOpen READ getProp_IsOpen WRITE setProp_IsOpen NOTIFY prop_IsOpenChanged FINAL)
};



#endif // PLUGIN_SERIALSERVER_H
