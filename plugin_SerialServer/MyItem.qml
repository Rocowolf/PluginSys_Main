import QtQuick 2.15
import QtQuick.Controls
import QtQuick.Controls.Fusion
import Plugin_SerialServer 1.0

Rectangle {
    id: root
    color: "purple"

    Row {
        property real prop_ChildWidth: width - spacing - 2*padding
        property real prop_ChildHeight: height - 2*padding
        width: parent.width
        height: parent.height
        padding: 4
        spacing: 10

        Rectangle {
            id: rect1
            height: parent.prop_ChildHeight
            width: Math.min(root.width * 0.4,300)
            radius: 10
            border.width: 2
            color: "orange"

            Column {
                id: layout
                property real prop_Width: width
                property real prop_CellHeight: 40
                property real prop_Padding: 6
                property real prop_ComboRatio: 0.6
                width: parent.width - 12
                height: parent.height - 12
                anchors.centerIn: parent
                spacing: 10

                Rectangle {//串口选择
                    width: layout.prop_Width
                    height: layout.prop_CellHeight
                    color: "gray"
                    radius: 8

                    Button {
                        id: txt

                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        width: 80
                        text: "串口选择"
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        enabled: !Plugin_SerialServer.prop_IsOpen
                        onClicked: {
                            combo1.model = Plugin_SerialServer.func_GetAvailableSerial()
                        }
                    }

                    ComboBox {
                        id: combo1
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.rightMargin: 10
                        model: []
                        focusPolicy: Qt.NoFocus
                        width: parent.width - txt.width - 30
                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        enabled: !Plugin_SerialServer.prop_IsOpen
                    }
                }

                Rectangle {//解析器选择
                    width: layout.prop_Width
                    height: layout.prop_CellHeight
                    color: "gray"
                    radius: 8

                    Button {
                        id: txt2

                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        width: 80
                        text: "解析器选择"
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        enabled: !Plugin_SerialServer.prop_IsOpen
                        onClicked: {
                            combo.model = Plugin_SerialServer.func_GetAvailableAnalyser()
                        }
                    }

                    ComboBox {
                        id: combo
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.rightMargin: 10
                        model: []
                        focusPolicy: Qt.NoFocus
                        width: parent.width - txt2.width - 30
                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        enabled: !Plugin_SerialServer.prop_IsOpen
                    }
                }

                Rectangle {//波特率选择
                    width: layout.prop_Width
                    height: layout.prop_CellHeight
                    color: "gray"
                    radius: 8

                    Text {
                        id: txt3

                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        font.pixelSize: height
                        text: "波特率"
                        fontSizeMode: Text.Fit
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                    }

                    ComboBox {
                        id: comb2

                        editable: true
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.rightMargin: 10
                        textRole: "text"
                        valueRole: "value"
                        currentIndex: 3
                        width: parent.width - txt3.width - 30
                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        model: ListModel {
                            id: model2
                            ListElement {text : "1200"  ; value: 1200  }
                            ListElement {text : "2400"  ; value: 2400  }
                            ListElement {text : "4800"  ; value: 4800  }
                            ListElement {text : "9600"  ; value: 9600  }
                            ListElement {text : "14400" ; value: 14400 }
                            ListElement {text : "19200" ; value: 19200 }
                            ListElement {text : "38400" ; value: 38400 }
                            ListElement {text : "43000" ; value: 43000 }
                            ListElement {text : "57600" ; value: 57600 }
                            ListElement {text : "76800" ; value: 76800 }
                            ListElement {text : "115200"; value: 115200}
                            ListElement {text : "128000"; value: 128000}
                            ListElement {text : "230400"; value: 230400}
                            ListElement {text : "256000"; value: 256000}
                        }

                        focusPolicy: Qt.NoFocus

                        onAccepted: {
                            if(find(editText) === -1){
                                model.append({text: editText, value: parseInt(editText)})
                                comb2.currentIndex = model2.count - 1
                                comb2.focus = false
                            }
                        }

                        enabled: !Plugin_SerialServer.prop_IsOpen
                    }
                }

                Rectangle {//停止位
                    width: layout.prop_Width
                    height: layout.prop_CellHeight
                    color: "gray"
                    radius: 8

                    Text {
                        id: txt4

                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        font.pixelSize: height
                        text: "停止位"
                        fontSizeMode: Text.Fit
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                    }

                    ComboBox {
                        id: comb3
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.rightMargin: 10
                        textRole: "text"
                        valueRole: "value"
                        model: ListModel {
                            ListElement {text : "1" ; value: 1}
                            ListElement {text : "1.5" ; value: 3}
                            ListElement {text : "2"; value: 2}
                        }
                        focusPolicy: Qt.NoFocus
                        width: parent.width - txt4.width - 30
                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        enabled: !Plugin_SerialServer.prop_IsOpen
                    }
                }

                Rectangle {//数据位
                    width: layout.prop_Width
                    height: layout.prop_CellHeight
                    color: "gray"
                    radius: 8

                    Text {
                        id: txt5

                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        font.pixelSize: height
                        text: "数据位"
                        fontSizeMode: Text.Fit
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                    }

                    ComboBox {
                        id: comb4
                        currentIndex: 3
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.rightMargin: 10
                        textRole: "text"
                        valueRole: "value"
                        model: ListModel {
                            ListElement {text : "5" ; value : 5}
                            ListElement {text : "6" ; value : 6}
                            ListElement {text : "7" ; value : 7}
                            ListElement {text : "8" ; value : 8}
                        }
                        focusPolicy: Qt.NoFocus
                        width: parent.width - txt5.width - 30
                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        enabled: !Plugin_SerialServer.prop_IsOpen
                    }
                }

                Rectangle {//校验位
                    width: layout.prop_Width
                    height: layout.prop_CellHeight
                    color: "gray"
                    radius: 8

                    Text {
                        id: txt6

                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        font.pixelSize: height
                        text: "校验位"
                        fontSizeMode: Text.Fit
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                    }

                    ComboBox {
                        id: comb5
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.rightMargin: 10
                        textRole: "text"
                        valueRole: "value"
                        currentIndex: 0
                        model: ListModel {
                            ListElement {text : "无校验" ; value: 0}
                            ListElement {text : "奇校验" ; value: 3}
                            ListElement {text : "偶校验" ; value: 2}
                        }
                        focusPolicy: Qt.NoFocus
                        width: parent.width - txt6.width - 30
                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        enabled: !Plugin_SerialServer.prop_IsOpen
                    }
                }

//                Rectangle {//解析器
//                    width: layout.prop_Width
//                    height: layout.prop_CellHeight
//                    color: "gray"
//                    radius: 8

//                    Text {
//                        height: layout.prop_CellHeight - 2*layout.prop_Padding
//                        font.pixelSize: height
//                        text: "解析器"
//                        fontSizeMode: Text.Fit
//                        anchors.verticalCenter: parent.verticalCenter
//                        anchors.left: parent.left
//                        anchors.leftMargin: 10
//                    }

//                    ComboBox {
//                        id: comb6
//                        anchors.verticalCenter: parent.verticalCenter
//                        anchors.right: parent.right
//                        anchors.rightMargin: 10
//                        textRole: "text"
//                        valueRole: "value"
//                        model: []
//                        focusPolicy: Qt.NoFocus
//                        width: 100
//                        height: layout.prop_CellHeight - 2*layout.prop_Padding
//                    }
//                }

                Rectangle {//串口开关
                    width: layout.prop_Width
                    height: layout.prop_CellHeight
                    color: "gray"
                    radius: 8

                    Button {
                        id: button2
                        text: "打开串口"
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        focusPolicy: Qt.NoFocus
                        width: 100
                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        enabled: !Plugin_SerialServer.prop_IsOpen

                        onClicked: {
                            Plugin_SerialServer.func_Set(combo1.currentText, combo.currentText, comb2.currentValue, comb4.currentValue, comb3.currentValue, comb5.currentValue)
                            Plugin_SerialServer.func_Open()
                        }
                    }

                    Button {
                        id: button3
                        text: "关闭串口"
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.rightMargin: 10
                        focusPolicy: Qt.NoFocus
                        width: 100
                        height: layout.prop_CellHeight - 2*layout.prop_Padding
                        enabled: Plugin_SerialServer.prop_IsOpen

                        onClicked: {
                            Plugin_SerialServer.func_Close()
                        }
                    }
                }
            }
        }

        Column {
            property real prop_ChildHeight: parent.prop_ChildHeight - spacing
            property real prop_ChildWidth: width
            width: parent.prop_ChildWidth - rect1.width
            height: parent.prop_ChildHeight
            spacing: 10

            Connections{
                target: Plugin_SerialServer
                function onSig_DataComplete(p_raw, p_data){
//                    textarea2.text += p_raw
                    textarea2.append(p_raw)
                    let str = ""
                    for(let item in p_data){
                        str += (item + ":" + p_data[item] + "\n")
                    }
                    textarea1.text = str
                }
            }

            Rectangle {
                id: rect3
                height: parent.prop_ChildHeight - rect2.height
                width: parent.prop_ChildWidth
                radius: 10
                border.width: 2
                color: "orange"

                ScrollView{
                    id: scrollview2
                    width: parent.width - 12
                    height: parent.height - 12
                    contentHeight: textarea2.implicitHeight
                    anchors.centerIn: parent

                    TextArea{
                        id: textarea2
                        color: "black"
                        text: ""
                        font.pixelSize: 15
                        readOnly: true
                        background: Rectangle{
                            opacity: 0
                        }
                    }

                    ScrollBar.vertical: ScrollBar {
                        parent: scrollview2
                        x: scrollview2.mirrored ? 0 : scrollview2.width - width
                        y: scrollview2.topPadding
                        height: scrollview2.availableHeight
                        active: true
                    }

                    ScrollBar.horizontal: ScrollBar{
                        visible: false
                    }
                }
            }

            Rectangle {
                id: rect2
                height: 200
                width: parent.prop_ChildWidth
                radius: 10
                border.width: 2
                color: "orange"

                ScrollView{
                    id: scrollview1
                    width: parent.width - 12
                    height: parent.height - 12
                    contentHeight: textarea1.implicitHeight
                    anchors.centerIn: parent

                    TextArea{
                        id: textarea1
                        color: "black"
                        text: ""
                        font.pixelSize: 15
                        readOnly: true
                        background: Rectangle{
                            opacity: 0
                        }
                    }

                    ScrollBar.vertical: ScrollBar {
                        parent: scrollview1
                        x: scrollview1.mirrored ? 0 : scrollview1.width - width
                        y: scrollview1.topPadding
                        height: scrollview1.availableHeight
                        active: true
                    }

                    ScrollBar.horizontal: ScrollBar{
                        visible: false
                    }
                }
            }
        }
    }
}
