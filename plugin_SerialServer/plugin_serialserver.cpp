#include "plugin_serialserver.h"

Plugin_SerialServer::Plugin_SerialServer()
{
    this->prop_CurrentSerial = new QSerialPort;

//    this->prop_TEST = new QFile("E:/Proj/Qt6/PluginLearn/123.pdf");
//    qDebug() << this->prop_TEST->open(QIODevice::ReadWrite);
//    if(this->prop_TEST->isOpen()){
//        this->prop_TEST2 = this->prop_TEST->readAll();
//    }

    this->prop_AnalyseList.insert("无",nullptr);

    this->prop_Info.insert("plugin_name","plugin_SerialServer");
    this->prop_Info.insert("plugin_version","1.0.D");
    this->prop_Info.insert("plugin_dependence", QJsonArray());
    QJsonObject m_qml;
    m_qml.insert("qml_name","串口模块");
    m_qml.insert("qml_qmlurl","qrc:/SerialServer/MyItem.qml");
    m_qml.insert("qml_imgurl","qrc:/SerialServer/serial.png");
    m_qml.insert("qml_pluginname",this->prop_Info["plugin_name"]);
    this->prop_Info.insert("plugin_qml", m_qml);

    this->prop_QMLIndex = qmlRegisterSingletonInstance<Plugin_SerialServer>("Plugin_SerialServer", 1, 0, "Plugin_SerialServer", this);

    connect(this->prop_CurrentSerial, SIGNAL(readyRead()), this, SLOT(slot_ReadyRead()));
}

QJsonObject Plugin_SerialServer::vfunc_GetInformation()
{
    return this->prop_Info;
}

int Plugin_SerialServer::vfunc_Run()
{
    qDebug() << "[plugin_SerialServer] Run";
    return 0;
}

int Plugin_SerialServer::vfunc_Exit()
{
   // QQmlPrivate::qmlunregister(QQmlPrivate::SingletonRegistration, this->prop_QMLIndex);
    qDebug() << "[plugin_SerialServer] Exit";
    return 0;
}

void Plugin_SerialServer::vfunc_Debug()
{
    qDebug().noquote() << "==========================================================";
    qDebug().noquote() << tr("[%1] DEBUG").arg(this->prop_Info["plugin_name"].toString());
    qDebug().noquote() << tr("插件版本: %1").arg(this->prop_Info["plugin_version"].toString());
    qDebug().noquote() << tr("插件依赖项:");
    QJsonArray m_arr = this->prop_Info["plugin_dependence"].toArray();
    if(m_arr.isEmpty()){
        qDebug().noquote() << tr("\t无");
    }
    else{
        foreach(auto m_item, m_arr){
            qDebug().noquote() << tr("\t%1").arg(m_item.toString());
        }
    }
    qDebug().noquote() << tr("插件QML信息:");
    if(this->prop_Info["plugin_qml"].toObject().isEmpty()){
        qDebug().noquote() << tr("\t无");
    }
    else{
        QJsonObject m_obj = this->prop_Info["plugin_qml"].toObject();
        for(auto m_item = m_obj.begin(); m_item != m_obj.end(); ++m_item){
            qDebug().noquote() << "\t" << m_item.key() << ":" << m_item.value().toString();
        }
    }
    qDebug().noquote() << "==========================================================";
}

bool Plugin_SerialServer::vfunc_Vertify(QPluginLoader* p_loader)
{
    return (qobject_cast<Plugin_SerialServer*>(p_loader->instance()) != nullptr);
}

void Plugin_SerialServer::vfunc_Init(const QMap<QString, QSharedPointer<QPluginLoader> >* p_pluginptr)
{
    return;
}

void Plugin_SerialServer::vfunc_Add(const QString& p_name, QJsonObject (*p_func)(QByteArray))
{
   this->prop_AnalyseList.insert(p_name,p_func);
}

void Plugin_SerialServer::vfunc_Remove(const QString& p_name)
{
   if(this->prop_CurrentAnalyse == this->prop_AnalyseList.value(p_name)){
        this->prop_CurrentAnalyse = nullptr;
   }
   this->prop_AnalyseList.remove(p_name);
}

QList<QString> Plugin_SerialServer::func_GetAvailableSerial()
{
   QList<QString> m_ret;
   foreach(auto m_item, QSerialPortInfo::availablePorts()){
        m_ret.append(m_item.portName());
   }
   // qDebug() << m_ret;
   return m_ret;
}

QList<QString> Plugin_SerialServer::func_GetAvailableAnalyser()
{
   return this->prop_AnalyseList.keys();
}

void Plugin_SerialServer::func_Open()
{
   this->setProp_IsOpen(this->prop_CurrentSerial->open(QIODevice::ReadWrite));
}

void Plugin_SerialServer::func_Close()
{
   this->prop_CurrentSerial->close();
   this->setProp_IsOpen(0);
}

void Plugin_SerialServer::func_Set(QString p_name, QString p_analyse, int p_baudrate, int p_databit, int p_stopbit, int p_parity)
{
    // qDebug() << p_name;
    // qDebug() << p_analyse;
    // qDebug() << p_baudrate;
    // qDebug() << p_databit;
    // qDebug() << p_stopbit;
    // qDebug() << p_parity;
    if(this->prop_CurrentSerial->isOpen()){
        return;
    }
    this->prop_CurrentSerial->setPortName(p_name);
    if(this->prop_AnalyseList.contains(p_analyse)){
        this->prop_CurrentAnalyse = this->prop_AnalyseList.value(p_analyse);
    }
    else{
        this->prop_CurrentAnalyse = nullptr;
    }
    this->prop_CurrentSerial->setBaudRate(QSerialPort::BaudRate(p_baudrate));
    this->prop_CurrentSerial->setDataBits(QSerialPort::DataBits(p_databit));
    this->prop_CurrentSerial->setStopBits(QSerialPort::StopBits(p_stopbit));
    this->prop_CurrentSerial->setParity(QSerialPort::Parity(p_parity));
}

 bool Plugin_SerialServer::getProp_IsOpen() const
 {
    return prop_IsOpen;
 }

 void Plugin_SerialServer::setProp_IsOpen(bool newProp_IsOpen)
 {
    if (prop_IsOpen == newProp_IsOpen)
         return;
    prop_IsOpen = newProp_IsOpen;
    emit prop_IsOpenChanged();
 }

void Plugin_SerialServer::slot_ReadyRead()
{
    QByteArray m_data = this->prop_CurrentSerial->readAll();
    auto i = QtConcurrent::run([this,m_data](){
        if(this->prop_CurrentAnalyse == nullptr){
//            qDebug() << m_data.toHex();
            emit this->sig_DataComplete(QString(m_data.toHex(' ')).toUpper(), QJsonObject());
        }
        else{
//            qDebug() << m_data.toHex();
            emit this->sig_DataComplete(QString(m_data.toHex(' ')).toUpper(), this->prop_CurrentAnalyse(m_data));
        }
    });
}

//QByteArray Plugin_SerialServer::func_GetAvailableSerial(const QByteArray&)
//{
//    QJsonObject m_ret;
//    foreach(auto m_item, QSerialPortInfo::availablePorts()){
//        m_ret.insert(m_item.portName(),m_item.description());
//    }
//    return QJsonDocument(m_ret).toJson();
//}

//QByteArray Plugin_SerialServer::func_SetSerial(const QByteArray& p_config)
//{
//    if(this->prop_CurrentSerial->isOpen()){
//        this->prop_CurrentSerial->close();
//    }

//    QJsonObject m_config = QJsonDocument::fromJson(p_config).object();
//    auto m_value = m_config.find("baudrate");
//    if(m_value != m_config.end()){
//        this->prop_CurrentSerial->setBaudRate(m_value.value().toInteger());
//    }
//    m_value = m_config.find("parity");
//    if(m_value != m_config.end()){
//        this->prop_CurrentSerial->setParity(QSerialPort::Parity(m_value->toInt()));
//    }
//    m_value = m_config.find("stopbit");
//    if(m_value != m_config.end()){
//        this->prop_CurrentSerial->setStopBits(QSerialPort::StopBits(m_value->toInt()));
//    }
//    m_value = m_config.find("databit");
//    if(m_value != m_config.end()){
//        this->prop_CurrentSerial->setDataBits(QSerialPort::DataBits(m_value->toInt()));
//    }
//    m_value = m_config.find("portname");
//    if(m_value != m_config.end()){
//        this->prop_CurrentSerial->setPortName(m_value->toString());
//    }

//    this->prop_CurrentSerial->open(QIODevice::ReadWrite);

//    return QByteArray();
//}

//QByteArray Plugin_SerialServer::func_GetSerial(const QByteArray& p_config)
//{
//    QJsonObject m_ret;
//    m_ret.insert("state",this->prop_CurrentSerial->isOpen());
//    m_ret.insert("portname",this->prop_CurrentSerial->portName());
//    m_ret.insert("baudrate",this->prop_CurrentSerial->baudRate());
//    m_ret.insert("parity",this->prop_CurrentSerial->parity());
//    m_ret.insert("stopbit",this->prop_CurrentSerial->stopBits());
//    m_ret.insert("databit",this->prop_CurrentSerial->dataBits());
//    return QJsonDocument(m_ret).toJson();
//}

//QByteArray Plugin_SerialServer::func_OperateSerial(const QByteArray& p_operate)
//{
//    QJsonObject m_config = QJsonDocument::fromJson(p_operate).object();
//    auto m_value = m_config.find("operate");
//    if(m_value != m_config.end()){
//        if(m_value.value().toBool()){
//            this->prop_CurrentSerial->open(QIODevice::ReadWrite);
//        }
//        else{
//            this->prop_CurrentSerial->close();
//        }
//    }
//    return QByteArray();
//}

//QByteArray Plugin_SerialServer::func_WriteSerial(const QByteArray& p_data)
//{
//    this->prop_CurrentSerial->write(p_data);
//    return QByteArray();
//}
