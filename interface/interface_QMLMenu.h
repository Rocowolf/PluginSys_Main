#ifndef INTERFACE_QMLMENU_H
#define INTERFACE_QMLMENU_H

#include <QtPlugin>
#include <QJsonObject>
#include "interface_Base.h"

class Interface_QMLMenu : public Interface_Base{

public:
//    virtual QJsonObject vfunc_GetInformation() = 0;
//    virtual int vfunc_Run() = 0;
//    virtual int vfunc_Exit() = 0;
//    virtual void vfunc_Debug() = 0;

    virtual void vfunc_InitQML(const QJsonArray& p_obj) = 0;
    virtual void vfunc_LoadQML(const QJsonArray& p_obj) = 0;
    virtual void vfunc_UnloadQML(const QJsonArray& p_obj) = 0;
};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(Interface_QMLMenu, "Rocowolf.QMLMenu")
QT_END_NAMESPACE

#endif // INTERFACE_QMLMENU_H
