#ifndef INTERFACE_SERIALSERVER_H
#define INTERFACE_SERIALSERVER_H

#include <QtPlugin>
#include <QJsonObject>
#include "interface_UserBase.h"

class Interface_SerialServer : public Interface_UserBase{

public:
    virtual void vfunc_Add(const QString& p_name, QJsonObject(*p_func)(QByteArray)) = 0;
    virtual void vfunc_Remove(const QString& p_name) = 0;
};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(Interface_SerialServer, "Rocowolf.User.SerialServer")
QT_END_NAMESPACE

#endif // INTERFACE_SERIALSERVER_H
