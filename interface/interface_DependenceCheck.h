#ifndef INTERFACE_DEPENDENCECHECK_H
#define INTERFACE_DEPENDENCECHECK_H

#include <QtPlugin>
#include <QJsonObject>
#include "interface_Base.h"

class Interface_DependenceCheck : public Interface_Base{

public:
//    virtual QJsonObject vfunc_GetInformation() = 0;
//    virtual int vfunc_Run() = 0;
//    virtual int vfunc_Exit() = 0;
//    virtual void vfunc_Debug() = 0;

    virtual QJsonValue vfunc_LoadPlugin(QJsonObject m_obj) = 0;
    virtual void vfunc_UnloadPlugin(QString p_name) = 0;
};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(Interface_DependenceCheck, "Rocowolf.DependenceCheck")
QT_END_NAMESPACE

#endif // INTERFACE_DEPENDENCECHECK_H
