#ifndef INTERFACE_SERVERPLUGIN_H
#define INTERFACE_SERVERPLUGIN_H

#include <QtPlugin>
#include <QJsonObject>

#include "interface_Base.h"

class Interface_ServerPlugin : public Interface_Base{

public:
//    virtual QJsonObject vfunc_GetInformation() = 0;
//    virtual int vfunc_Run() = 0;
//    virtual int vfunc_Exit() = 0;
//    virtual void vfunc_Debug() = 0;
    virtual void vfunc_Init(const QMap<QString, QSharedPointer<QPluginLoader>>* p_pluginmap, const QStringList* p_filelist) = 0;

    virtual void vsig_Downloaded(QString p_name) = 0;
    virtual void vsig_Buffered(QString p_name, QByteArray p_data) = 0;
    virtual void vsig_Aborted(QString p_name) = 0;
    virtual void vsig_Update(QString p_name, qint64 p_current, qint64 p_total) = 0;
    virtual void vsig_Installed(QString p_name, bool p_state) = 0;
    virtual void vsig_Uninstalled(QString p_name, bool p_state) = 0;

    virtual void vsig_Install(QString p_name) = 0;
    virtual void vsig_Uninstall(QString p_name) = 0;
    virtual void vsig_Download(QString p_name) = 0;
    virtual void vsig_Abort(QString p_name) = 0;
};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(Interface_ServerPlugin, "Rocowolf.ServerPlugin")
QT_END_NAMESPACE

#endif // INTERFACE_SERVERPLUGIN_H
