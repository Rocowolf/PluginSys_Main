#ifndef INTERFACE_USERBASE_H
#define INTERFACE_USERBASE_H

#include <QtPlugin>
#include <QJsonObject>
#include "interface_Base.h"

class Interface_UserBase : public Interface_Base{

public:
    //传入所有被加载的插件，选出依赖项在类内创建指向其的SharedPointer
    virtual void vfunc_Init(const QMap<QString,QSharedPointer<QPluginLoader>>* p_pluginptr) = 0;

    //待验证，也许可以通过Init中绑定信号的方法代替，然后在manager模块创建智能指针的deletor中调用manager的slot_uninstall函数
    virtual void vsig_Unload(QString p_name) = 0;
};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(Interface_UserBase, "Rocowolf.UserPlugin")
QT_END_NAMESPACE

#endif // INTERFACE_USERBASE_H
