#ifndef INTERFACE_BASE_H
#define INTERFACE_BASE_H

#include <QtPlugin>
#include <QJsonObject>
#include <QSharedPointer>
#include <QPluginLoader>
#include <QObject>

class Interface_Base{

public:
    virtual QJsonObject vfunc_GetInformation() = 0;
    virtual bool vfunc_Vertify(QPluginLoader* p_loader) = 0;
    virtual int vfunc_Run() = 0;
    virtual int vfunc_Exit() = 0;
    virtual void vfunc_Debug() = 0;
};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(Interface_Base, "Rocowolf.Plugin")
QT_END_NAMESPACE

#endif // INTERFACE_BASE_H
